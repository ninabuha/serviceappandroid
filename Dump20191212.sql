CREATE DATABASE  IF NOT EXISTS `bobandb` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `bobandb`;
-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: bobandb
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `akcija`
--

DROP TABLE IF EXISTS `akcija`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `akcija` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv_akcije` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `akcija`
--

LOCK TABLES `akcija` WRITE;
/*!40000 ALTER TABLE `akcija` DISABLE KEYS */;
INSERT INTO `akcija` VALUES (17,'Zamena amortizera'),(18,'Popravka motora'),(19,'Zamena ulja'),(20,'Popravka amortizera'),(21,'Zamena sofersajbne'),(22,'Popravka menjaca'),(23,'Popravka volana');
/*!40000 ALTER TABLE `akcija` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `automobil`
--

DROP TABLE IF EXISTS `automobil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `automobil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Model_id` int(11) NOT NULL,
  `broj_sasije` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tablice_UNIQUE` (`broj_sasije`),
  KEY `fk_Automobil_Model1_idx` (`Model_id`),
  CONSTRAINT `fk_Automobil_Model1` FOREIGN KEY (`Model_id`) REFERENCES `model` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `automobil`
--

LOCK TABLES `automobil` WRITE;
/*!40000 ALTER TABLE `automobil` DISABLE KEYS */;
INSERT INTO `automobil` VALUES (50,295,'JDJJDSH788DS78899'),(51,258,'B77DB8DS89D88F7SS88FS88FS'),(52,171,'TETE7S7D8S89CD89DSDS'),(53,957,'NMMVNVK0VNIV8C87C8'),(54,297,'BBU7ZGZJCJG7HGUFU7GZG7'),(55,250,'VGH4SR3FG9G6R564EGBU'),(56,299,'NJDKSN7VNRD848E9HVD'),(57,885,'VGHVGH5RFT766VU6'),(58,889,'BJBZ67T6ZV576TGV87G'),(59,224,'JSBKDES7ES8ES');
/*!40000 ALTER TABLE `automobil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `izvrsenaakcija`
--

DROP TABLE IF EXISTS `izvrsenaakcija`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `izvrsenaakcija` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cena_izvrsene_akcije` float NOT NULL,
  `Serviser_id` int(11) NOT NULL,
  `Akcija_id` int(11) NOT NULL,
  `RadniNalog_id` int(11) NOT NULL,
  `uspesna` tinyint(1) NOT NULL,
  `datum_izvrsavanja` datetime NOT NULL,
  `opis_akcije` varchar(2000) DEFAULT NULL,
  `uneo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_IzvrsenaAkcija_Serviser1_idx` (`Serviser_id`),
  KEY `fk_IzvrsenaAkcija_Akcija1_idx` (`Akcija_id`),
  KEY `fk_IzvrsenaAkcija_RadniNalog1_idx` (`RadniNalog_id`),
  CONSTRAINT `fk_IzvrsenaAkcija_Akcija1` FOREIGN KEY (`Akcija_id`) REFERENCES `akcija` (`id`),
  CONSTRAINT `fk_IzvrsenaAkcija_RadniNalog1` FOREIGN KEY (`RadniNalog_id`) REFERENCES `radninalog` (`id`),
  CONSTRAINT `fk_IzvrsenaAkcija_Serviser1` FOREIGN KEY (`Serviser_id`) REFERENCES `serviser` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `izvrsenaakcija`
--

LOCK TABLES `izvrsenaakcija` WRITE;
/*!40000 ALTER TABLE `izvrsenaakcija` DISABLE KEYS */;
INSERT INTO `izvrsenaakcija` VALUES (69,5600,1,20,12522,1,'2019-10-11 00:07:20',NULL,1),(70,1500,2,19,12522,1,'2019-10-11 00:07:44',NULL,2),(71,16000,3,22,12523,1,'2019-10-11 00:09:51',NULL,1),(72,36000,2,18,12521,1,'2019-10-11 00:10:56',NULL,1),(73,1000,33,19,12525,1,'2019-10-11 00:11:39',NULL,1),(74,14000,33,21,12525,1,'2019-10-11 00:11:56',NULL,1),(75,5100,3,18,12526,1,'2019-10-11 00:12:45',NULL,1),(76,24000,33,23,12524,1,'2019-10-11 00:15:42',NULL,1),(77,3000,1,22,12530,1,'2019-10-11 00:36:56','test',3),(78,6200,3,23,12531,1,'2019-10-11 15:35:28','Opis akcije',1),(79,1250,33,21,12531,1,'2019-10-11 15:36:15',NULL,1),(80,12000,2,21,12529,1,'2019-10-11 16:28:50',NULL,1),(81,350,33,23,12529,0,'2019-10-11 16:43:04',NULL,1),(82,12600,3,17,12528,0,'2019-10-11 16:49:23','Zamenjeni amortizeri...',1),(83,4000,1,21,12529,1,'2019-10-11 17:02:41','Detaljan opis ove izvrsene akcije... sajbdakudanu dajbdabbdubaod',1),(84,1200,1,20,12526,1,'2019-10-12 20:56:05',NULL,1),(85,12600,1,21,12529,1,'2019-10-12 21:16:28',NULL,1),(86,2000,2,19,12532,1,'2019-10-17 18:08:50',NULL,1);
/*!40000 ALTER TABLE `izvrsenaakcija` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `klijent`
--

DROP TABLE IF EXISTS `klijent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `klijent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(100) NOT NULL,
  `prezime` varchar(100) NOT NULL,
  `telefon` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `telefon_UNIQUE` (`telefon`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `klijent`
--

LOCK TABLES `klijent` WRITE;
/*!40000 ALTER TABLE `klijent` DISABLE KEYS */;
INSERT INTO `klijent` VALUES (29,'Marko','Marković','06055698875'),(30,'Dragan','Draganović','06488779558'),(31,'Saša','Simić','0647784125555'),(32,'Bojana','Bojić','06026698887'),(33,'Milan','Milović','0647741124'),(34,'Višnja','Višnjić','065488799997');
/*!40000 ALTER TABLE `klijent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model`
--

DROP TABLE IF EXISTS `model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv_modela` varchar(45) NOT NULL,
  `Proizvodjac_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Model_Proizvodjac1_idx` (`Proizvodjac_id`),
  CONSTRAINT `fk_Model_Proizvodjac1` FOREIGN KEY (`Proizvodjac_id`) REFERENCES `proizvodjac` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1367 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model`
--

LOCK TABLES `model` WRITE;
/*!40000 ALTER TABLE `model` DISABLE KEYS */;
INSERT INTO `model` VALUES (153,'33',1),(154,'75',1),(155,'145',1),(156,'146',1),(157,'147',1),(158,'155',1),(159,'156',1),(160,'156 Crosswagon',1),(161,'159',1),(162,'164',1),(163,'166',1),(164,'Alfasud',1),(165,'Brera',1),(166,'Giulia',1),(167,'Giulietta',1),(168,'GT',1),(169,'GTV',1),(170,'MiTo',1),(171,'Spider',1),(172,'Sprint',1),(173,'Stelvio',1),(174,'Ostalo',1),(175,'Serija 10',2),(176,'Ostalo',2),(177,'DB 9',3),(178,'Ostalo',3),(179,'80',4),(180,'90',4),(181,'100',4),(182,'200',4),(183,'A1',4),(184,'A2',4),(185,'A3',4),(186,'A4',4),(187,'A4 Allroad',4),(188,'A5',4),(189,'A6',4),(190,'A6 Allroad',4),(191,'A7',4),(192,'A8',4),(193,'Coupe',4),(194,'Q2',4),(195,'Q3',4),(196,'Q5',4),(197,'Q7',4),(198,'Q8',4),(199,'R8',4),(200,'RS4',4),(201,'RS6',4),(202,'RS Q3',4),(203,'S2',4),(204,'S3',4),(205,'S4',4),(206,'S5',4),(207,'S8',4),(208,'SQ5',4),(209,'SQ7',4),(210,'TT',4),(211,'Ostalo',4),(212,'Mini',5),(213,'Moris Marina',5),(214,'Ostalo',5),(215,'Continental',6),(216,'Ostalo',6),(217,'Serija 1',7),(218,'114',7),(219,'116',7),(220,'118',7),(221,'120',7),(222,'123',7),(223,'125',7),(224,'135',7),(225,'Serija 2',7),(226,'216',7),(227,'218',7),(228,'220',7),(229,'Serija 3',7),(230,'315',7),(231,'316',7),(232,'318',7),(233,'320',7),(234,'323',7),(235,'324',7),(236,'325',7),(237,'328',7),(238,'330',7),(239,'335',7),(240,'Compact',7),(241,'Serija 3 GT',7),(242,'318 GT',7),(243,'320 GT',7),(244,'Serija 4',7),(245,'418',7),(246,'420',7),(247,'428',7),(248,'430',7),(249,'435',7),(250,'Serija 5',7),(251,'518',7),(252,'520',7),(253,'523',7),(254,'524',7),(255,'525',7),(256,'528',7),(257,'530',7),(258,'535',7),(259,'540',7),(260,'545',7),(261,'M550',7),(262,'Serija 5 GT',7),(263,'520 GT',7),(264,'530 GT',7),(265,'Serija 6',7),(266,'630',7),(267,'640',7),(268,'645',7),(269,'650',7),(270,'Serija 7',7),(271,'725',7),(272,'728',7),(273,'730',7),(274,'735',7),(275,'740',7),(276,'745',7),(277,'750',7),(278,'760',7),(279,'Serija 8',7),(280,'840',7),(281,'Serija M',7),(282,'M3',7),(283,'M4',7),(284,'M5',7),(285,'M6',7),(286,'Serija X',7),(287,'X1',7),(288,'X2',7),(289,'X3',7),(290,'X4',7),(291,'X5',7),(292,'X6',7),(293,'X7',7),(294,'Serija i',7),(295,'i3',7),(296,'Serija Z',7),(297,'Z3',7),(298,'Z4',7),(299,'2002',7),(300,'Ostalo',7),(301,'Park Avenue',8),(302,'Ostalo',8),(303,'Escalade',9),(304,'Ostalo',9),(305,'Ego',10),(306,'Tengo',10),(307,'Tiggo',10),(308,'Ostalo',10),(309,'Aveo',11),(310,'Blazer',11),(311,'Caprice',11),(312,'Captiva',11),(313,'Corsica',11),(314,'Corvette',11),(315,'Cruze',11),(316,'Epica',11),(317,'Evanda',11),(318,'Impala',11),(319,'Kalos',11),(320,'Lacetti',11),(321,'Lumina',11),(322,'Matiz',11),(323,'Nubira',11),(324,'Orlando',11),(325,'Spark',11),(326,'Tacuma',11),(327,'Tahoe',11),(328,'Trailblazer',11),(329,'Trax',11),(330,'Venture',11),(331,'Ostalo',11),(332,'300M',12),(333,'300C',12),(334,'Crossfire',12),(335,'Grand Voyager',12),(336,'Le Baron',12),(337,'Neon',12),(338,'Pacifica',12),(339,'PT Cruiser',12),(340,'Sebring',12),(341,'Stratus',12),(342,'Sunbeam',12),(343,'Town & Country',12),(344,'Voyager',12),(345,'Ostalo',12),(346,'2CV',13),(347,'Ami',13),(348,'AX',13),(349,'Berlingo',13),(350,'BX',13),(351,'C-Crosser',13),(352,'C-ELYSEE',13),(353,'C1',13),(354,'C2',13),(355,'C3',13),(356,'C3 Aircross',13),(357,'C3 Picasso',13),(358,'C3 pluriel',13),(359,'C4',13),(360,'C4 Aircross',13),(361,'C4 Cactus',13),(362,'C4 Grand Picasso',13),(363,'C4 Picasso',13),(364,'C4 SpaceTourer',13),(365,'C5',13),(366,'C5 Aircross',13),(367,'C6',13),(368,'C8',13),(369,'C15',13),(370,'CX',13),(371,'DS',13),(372,'DS3',13),(373,'DS4',13),(374,'DS5',13),(375,'DS7',13),(376,'Dyane',13),(377,'Evasion',13),(378,'GS',13),(379,'Jumpy',13),(380,'Nemo',13),(381,'Saxo',13),(382,'Visa',13),(383,'Xantia',13),(384,'XM',13),(385,'Xsara',13),(386,'Xsara Picasso',13),(387,'ZX',13),(388,'Ostalo',13),(389,'1307',14),(390,'1310',14),(391,'Aro',14),(392,'Dokker',14),(393,'Double',14),(394,'Duster',14),(395,'Lodgy',14),(396,'Logan',14),(397,'Nova',14),(398,'Pickup',14),(399,'Sandero',14),(400,'Solenza',14),(401,'Stepway',14),(402,'Super Nova',14),(403,'Ostalo',14),(404,'Espero',15),(405,'Evanda',15),(406,'Kalos',15),(407,'Lacetti',15),(408,'Lanos',15),(409,'Leganza',15),(410,'Matiz',15),(411,'Nexia',15),(412,'Nubira',15),(413,'Tacuma',15),(414,'Tico',15),(415,'Ostalo',15),(416,'Applause',16),(417,'Charade',16),(418,'Cuore',16),(419,'Feroza',16),(420,'Gran Move',16),(421,'Materia',16),(422,'Move',16),(423,'Rocky',16),(424,'Sirion',16),(425,'Terios',16),(426,'YRV',16),(427,'Ostalo',16),(428,'Avenger',17),(429,'Caliber',17),(430,'Grand Caravan',17),(431,'Intrepid',17),(432,'Journey',17),(433,'Neon',17),(434,'Nitro',17),(435,'RAM',17),(436,'Ostalo',17),(437,'D5',18),(438,'Ostalo',18),(439,'599',19),(440,'Ostalo',19),(441,'124',20),(442,'125',20),(443,'126',20),(444,'127',20),(445,'131',20),(446,'132',20),(447,'500',20),(448,'500C',20),(449,'500L',20),(450,'500X',20),(451,'600',20),(452,'850',20),(453,'1100',20),(454,'1107',20),(455,'1300',20),(456,'Albea',20),(457,'Barchetta',20),(458,'Brava',20),(459,'Bravo',20),(460,'Campagnola',20),(461,'Cinquecento',20),(462,'Coupe',20),(463,'Croma',20),(464,'Doblo',20),(465,'EVO',20),(466,'Fiorino',20),(467,'Freemont',20),(468,'Grande Punto',20),(469,'Idea',20),(470,'Linea',20),(471,'Marea',20),(472,'Marengo',20),(473,'Multipla',20),(474,'Palio',20),(475,'Panda',20),(476,'Punto',20),(477,'Qubo',20),(478,'Scudo',20),(479,'Sedici',20),(480,'Seicento',20),(481,'Spider Europa',20),(482,'Stilo',20),(483,'Tempra',20),(484,'Tipo',20),(485,'Ulysse',20),(486,'Uno',20),(487,'x 1/9',20),(488,'Ostalo',20),(489,'Aerostar',21),(490,'B-Max',21),(491,'C-Max',21),(492,'Capri',21),(493,'Courier',21),(494,'Eco Sport',21),(495,'Edge',21),(496,'Escort',21),(497,'Excursion',21),(498,'Expedition',21),(499,'Explorer',21),(500,'F 150',21),(501,'Festiva',21),(502,'Fiesta',21),(503,'Focus',21),(504,'Fusion',21),(505,'Galaxy',21),(506,'Granada',21),(507,'Grand C-Max',21),(508,'Ka',21),(509,'Kuga',21),(510,'Maverick',21),(511,'Mondeo',21),(512,'Mustang',21),(513,'Orion',21),(514,'Probe',21),(515,'Puma',21),(516,'Ranger',21),(517,'S-Max',21),(518,'Scorpio',21),(519,'Sierra',21),(520,'Street Ka',21),(521,'Taunus',21),(522,'Taurus',21),(523,'Tourneo',21),(524,'Tourneo Connect',21),(525,'Tourneo Courier',21),(526,'Ostalo',21),(527,'3102',22),(528,'Ostalo',22),(529,'H6',23),(530,'Steed',23),(531,'Steed 5',23),(532,'Ostalo',23),(533,'Yukon',24),(534,'Ostalo',24),(535,'Accord',25),(536,'Civic',25),(537,'Concerto',25),(538,'CR-V',25),(539,'CR-Z',25),(540,'CRX',25),(541,'FR-V',25),(542,'HR-V',25),(543,'Insight',25),(544,'Integra',25),(545,'Jazz',25),(546,'Legend',25),(547,'Prelude',25),(548,'Shuttle',25),(549,'Stream',25),(550,'Ostalo',25),(551,'H1',26),(552,'H2',26),(553,'Ostalo',26),(554,'Accent',27),(555,'Atos',27),(556,'Coupe',27),(557,'Elantra',27),(558,'Galloper',27),(559,'Genesis',27),(560,'Getz',27),(561,'H 1',27),(562,'H 100',27),(563,'i10',27),(564,'i20',27),(565,'i30',27),(566,'i40',27),(567,'ix20',27),(568,'ix35',27),(569,'ix55',27),(570,'Kona',27),(571,'Lantra',27),(572,'Matrix',27),(573,'Pony',27),(574,'S-Coupe',27),(575,'Santa Fe',27),(576,'Santamo',27),(577,'Sonata',27),(578,'Sonica',27),(579,'Terracan',27),(580,'Trajet',27),(581,'Tucson',27),(582,'Veloster',27),(583,'Ostalo',27),(584,'EX30',28),(585,'FX30',28),(586,'FX35',28),(587,'Q30',28),(588,'Q50',28),(589,'Q70',28),(590,'Ostalo',28),(591,'D-Max',29),(592,'Trooper',29),(593,'Ostalo',29),(594,'F pace',30),(595,'S-Type',30),(596,'X-Type',30),(597,'XE',30),(598,'XF',30),(599,'XJ',30),(600,'XJ6',30),(601,'Ostalo',30),(602,'Cherokee',31),(603,'CJ',31),(604,'Commander',31),(605,'Compass',31),(606,'Grand Cherokee',31),(607,'Liberty',31),(608,'Patriot',31),(609,'Renegade',31),(610,'Wagoneer',31),(611,'Willys',31),(612,'Wrangler',31),(613,'Ostalo',31),(614,'Carens',32),(615,'Carnival',32),(616,'cee`d',32),(617,'cee`d sw',32),(618,'Cerato',32),(619,'Clarus',32),(620,'Joice',32),(621,'Magentis',32),(622,'Optima',32),(623,'Picanto',32),(624,'Pride',32),(625,'pro_cee`d',32),(626,'Rio',32),(627,'Rocsta',32),(628,'Sephia',32),(629,'Shuma',32),(630,'Sorento',32),(631,'Soul',32),(632,'Spectra',32),(633,'Sportage',32),(634,'Stonic',32),(635,'Ostalo',32),(636,'110',33),(637,'111',33),(638,'112',33),(639,'1200',33),(640,'1300',33),(641,'1500',33),(642,'1600',33),(643,'2101',33),(644,'2103',33),(645,'2104',33),(646,'2105',33),(647,'2107',33),(648,'Aleko',33),(649,'Kalina',33),(650,'Niva',33),(651,'Riva',33),(652,'Samara',33),(653,'Vesta',33),(654,'Ostalo',33),(655,'Aventador',34),(656,'Gallardo',34),(657,'Ostalo',34),(658,'Beta',35),(659,'Dedra',35),(660,'Delta',35),(661,'Kappa',35),(662,'Lybra',35),(663,'Musa',35),(664,'Phedra',35),(665,'Prisma',35),(666,'Thema',35),(667,'Thesis',35),(668,'Ypsilon',35),(669,'Zeta',35),(670,'Ostalo',35),(671,'Defender',36),(672,'Discovery',36),(673,'Freelander',36),(674,'Range Rover',36),(675,'Range Rover Evoque',36),(676,'Range Rover Sport',36),(677,'Range Rover Velar',36),(678,'Range Rover Vogue',36),(679,'Serija II',36),(680,'Serija III',36),(681,'Ostalo',36),(682,'CT 200h',37),(683,'GS',37),(684,'GS 450',37),(685,'IS',37),(686,'IS 200',37),(687,'IS 220',37),(688,'IS 250',37),(689,'LS',37),(690,'LS 400',37),(691,'LS 460',37),(692,'RX',37),(693,'RX 300',37),(694,'RX 400',37),(695,'Ostalo',37),(696,'Navigator',38),(697,'Town car',38),(698,'Ostalo',38),(699,'CJ',39),(700,'Goa',39),(701,'Ostalo',39),(702,'Ghibli',40),(703,'Granturismo',40),(704,'Levante',40),(705,'Quattroporte',40),(706,'Ostalo',40),(707,'2',41),(708,'3',41),(709,'5',41),(710,'6',41),(711,'121',41),(712,'323',41),(713,'626',41),(714,'929',41),(715,'B 250',41),(716,'BT-50',41),(717,'CX-3',41),(718,'CX-5',41),(719,'CX-7',41),(720,'Demio',41),(721,'MPV',41),(722,'MX-3',41),(723,'MX-5',41),(724,'Premacy',41),(725,'Protege',41),(726,'RX-8',41),(727,'Tribute',41),(728,'Xedos',41),(729,'Ostalo',41),(730,'A Klasa',42),(731,'A 140',42),(732,'A 150',42),(733,'A 160',42),(734,'A 170',42),(735,'A 180',42),(736,'A 190',42),(737,'A 200',42),(738,'A 210',42),(739,'A 220',42),(740,'A 250',42),(741,'A 45 AMG',42),(742,'B Klasa',42),(743,'B 150',42),(744,'B 160',42),(745,'B 170',42),(746,'B 180',42),(747,'B 200',42),(748,'C Klasa',42),(749,'C 180',42),(750,'C 200',42),(751,'C 220',42),(752,'C 230',42),(753,'C 240',42),(754,'C 250',42),(755,'C 270',42),(756,'C 280',42),(757,'C 320',42),(758,'C 63 AMG',42),(759,'CE Klasa',42),(760,'CE 230',42),(761,'Citan',42),(762,'CLA Klasa',42),(763,'CLA 180',42),(764,'CLA 200',42),(765,'CLA 220',42),(766,'CL Klasa',42),(767,'CL 500',42),(768,'CL 55 AMG',42),(769,'CL 63 AMG',42),(770,'CLC Klasa',42),(771,'CLC 220',42),(772,'CLK Klasa',42),(773,'CLK 200',42),(774,'CLK 220',42),(775,'CLK 230',42),(776,'CLK 240',42),(777,'CLK 270',42),(778,'CLK 320',42),(779,'CLK 500',42),(780,'CLS Klasa',42),(781,'CLS 220',42),(782,'CLS 250',42),(783,'CLS 300',42),(784,'CLS 320',42),(785,'CLS 350',42),(786,'CLS 400',42),(787,'CLS 500',42),(788,'CLS 55 AMG',42),(789,'E Klasa',42),(790,'E 200',42),(791,'E 220',42),(792,'E 230',42),(793,'E 240',42),(794,'E 250',42),(795,'E 260',42),(796,'E 270',42),(797,'E 280',42),(798,'E 290',42),(799,'E 300',42),(800,'E 320',42),(801,'E 350',42),(802,'E 400',42),(803,'E 500',42),(804,'G Klasa',42),(805,'G 230',42),(806,'G 250',42),(807,'G 290',42),(808,'G 300',42),(809,'G 320',42),(810,'G 350',42),(811,'G 400',42),(812,'G 500',42),(813,'G 63 AMG',42),(814,'GL Klasa',42),(815,'GL 320',42),(816,'GL 350',42),(817,'GL 450',42),(818,'GL 500',42),(819,'GLA Klasa',42),(820,'GLA 45 AMG',42),(821,'GLA 180',42),(822,'GLA 200',42),(823,'GLA 220',42),(824,'GLC Klasa',42),(825,'GLC 220',42),(826,'GLC 250',42),(827,'GLC 350',42),(828,'GLE Klasa',42),(829,'GLE 250',42),(830,'GLE 300',42),(831,'GLE 350',42),(832,'GLK Klasa',42),(833,'GLK 200',42),(834,'GLK 220',42),(835,'GLK 250',42),(836,'GLK 320',42),(837,'GLS Klasa',42),(838,'GLS 350 D',42),(839,'GLS 400',42),(840,'MB 100',42),(841,'ML Klasa',42),(842,'ML 230',42),(843,'ML 250',42),(844,'ML 270',42),(845,'ML 280',42),(846,'ML 300',42),(847,'ML 320',42),(848,'ML 350',42),(849,'ML 400',42),(850,'ML 430',42),(851,'ML 500',42),(852,'ML 55 AMG',42),(853,'ML 63 AMG',42),(854,'R Klasa',42),(855,'R 320',42),(856,'R 350',42),(857,'S Klasa',42),(858,'S 250',42),(859,'S 280',42),(860,'S 300',42),(861,'S 320',42),(862,'S 350',42),(863,'S 400',42),(864,'S 420',42),(865,'S 500',42),(866,'S 55',42),(867,'S 550',42),(868,'S 600',42),(869,'S 63 AMG',42),(870,'SL Klasa',42),(871,'SL 350',42),(872,'SL 380',42),(873,'SL 500',42),(874,'SL 63 AMG',42),(875,'SLK Klasa',42),(876,'SLK 200',42),(877,'SLK 230',42),(878,'SLK 250',42),(879,'Vaneo',42),(880,'180',42),(881,'190',42),(882,'Ostalo',42),(883,'1000',43),(884,'Clubman',43),(885,'Cooper',43),(886,'Cooper S',43),(887,'Countryman',43),(888,'Paceman',43),(889,'One',43),(890,'Ostalo',43),(891,'MGF',44),(892,'TF',44),(893,'ZR',44),(894,'ZS',44),(895,'ZT',44),(896,'Ostalo',44),(897,'ASX',45),(898,'Carisma',45),(899,'Colt',45),(900,'Eclipse',45),(901,'Endeavor',45),(902,'Galant',45),(903,'Grandis',45),(904,'L200',45),(905,'L300',45),(906,'Lancer',45),(907,'Montero',45),(908,'Outlander',45),(909,'Pajero',45),(910,'Pajero Pinin',45),(911,'Sigma',45),(912,'Space Gear',45),(913,'Space Runner',45),(914,'Space Star',45),(915,'Space Wagon',45),(916,'Ostalo',45),(917,'2137',46),(918,'2140',46),(919,'Aleko 2141',46),(920,'Ostalo',46),(921,'300 ZX',47),(922,'350Z',47),(923,'Almera',47),(924,'Almera Tino',47),(925,'Altima',47),(926,'Bluebird',47),(927,'Juke',47),(928,'Kubistar',47),(929,'Maxima',47),(930,'Micra',47),(931,'Murano',47),(932,'Navara',47),(933,'Note',47),(934,'Pathfinder',47),(935,'Patrol',47),(936,'Pixo',47),(937,'Praire',47),(938,'Primastar',47),(939,'Primera',47),(940,'Pulsar',47),(941,'Qashqai',47),(942,'Qashqai + 2',47),(943,'Sentra',47),(944,'Serena',47),(945,'Sunny',47),(946,'Terrano',47),(947,'Tiida',47),(948,'Vanette',47),(949,'X-Trail',47),(950,'Xterra',47),(951,'Ostalo',47),(952,'Ostalo',48),(953,'Adam',49),(954,'Agila',49),(955,'Antara',49),(956,'Ascona',49),(957,'Astra',49),(958,'Astra F',49),(959,'Astra G',49),(960,'Astra H',49),(961,'Astra J',49),(962,'Astra K',49),(963,'Calibra',49),(964,'Cascada',49),(965,'Combo',49),(966,'Corsa',49),(967,'Corsa A',49),(968,'Corsa B',49),(969,'Corsa C',49),(970,'Corsa D',49),(971,'Corsa E',49),(972,'Crosland X ',49),(973,'Frontera',49),(974,'GT',49),(975,'Grandland X',49),(976,'Insignia',49),(977,'Kadett',49),(978,'Manta',49),(979,'Meriva',49),(980,'Monterey',49),(981,'Mokka',49),(982,'Mokka X',49),(983,'Olympia',49),(984,'Omega',49),(985,'Rekord',49),(986,'Senator',49),(987,'Signum',49),(988,'Sintra',49),(989,'Tigra',49),(990,'Vectra',49),(991,'Vectra A',49),(992,'Vectra B',49),(993,'Vectra C',49),(994,'Zafira',49),(995,'Ostalo',49),(996,'106',50),(997,'107',50),(998,'108',50),(999,'204',50),(1000,'205',50),(1001,'206',50),(1002,'207',50),(1003,'208',50),(1004,'301',50),(1005,'304',50),(1006,'305',50),(1007,'306',50),(1008,'307',50),(1009,'308',50),(1010,'309',50),(1011,'404',50),(1012,'405',50),(1013,'406',50),(1014,'407',50),(1015,'504',50),(1016,'508',50),(1017,'604',50),(1018,'605',50),(1019,'607',50),(1020,'806',50),(1021,'807',50),(1022,'1007',50),(1023,'2008',50),(1024,'3008',50),(1025,'4007',50),(1026,'4008',50),(1027,'5008',50),(1028,'Bipper',50),(1029,'Expert',50),(1030,'Partner',50),(1031,'Ranch',50),(1032,'RCZ',50),(1033,'Rifter',50),(1034,'TePee',50),(1035,'Ostalo',50),(1036,'Porter',51),(1037,'Ostalo',51),(1038,'Caro',52),(1039,'Ostalo',52),(1040,'125 P',53),(1041,'126 P',53),(1042,'Ostalo',53),(1043,'Firebird',54),(1044,'Trans Am',54),(1045,'Ostalo',54),(1046,'911',55),(1047,'924',55),(1048,'944',55),(1049,'Boxster',55),(1050,'Cayenne',55),(1051,'Macan',55),(1052,'Panamera',55),(1053,'Ostalo',55),(1054,'Serija 400',56),(1055,'Ostalo',56),(1056,'Avantime',57),(1057,'Captur',57),(1058,'Clio',57),(1059,'Espace',57),(1060,'Express',57),(1061,'Fluence',57),(1062,'Grand Espace',57),(1063,'Grand Modus',57),(1064,'Grand Scenic',57),(1065,'Kadjar',57),(1066,'Kangoo',57),(1067,'Koleos',57),(1068,'Laguna',57),(1069,'Latitude',57),(1070,'Megane',57),(1071,'Modus',57),(1072,'Nevada',57),(1073,'R 4',57),(1074,'R 5',57),(1075,'R 9',57),(1076,'R 10',57),(1077,'R 11',57),(1078,'R 12',57),(1079,'R 18',57),(1080,'R 19',57),(1081,'R 20',57),(1082,'R 21',57),(1083,'R 25',57),(1084,'Rapid',57),(1085,'RX',57),(1086,'Safrane',57),(1087,'Scenic',57),(1088,'Talisman',57),(1089,'Thalia',57),(1090,'Twingo',57),(1091,'Vel Satis',57),(1092,'Wind',57),(1093,'Ostalo',57),(1094,'Flying Spur',58),(1095,'Ostalo',58),(1096,'200',59),(1097,'214',59),(1098,'216',59),(1099,'25',59),(1100,'400',59),(1101,'414',59),(1102,'416',59),(1103,'45',59),(1104,'600',59),(1105,'618',59),(1106,'620',59),(1107,'75',59),(1108,'820',59),(1109,'825',59),(1110,'Streetwise',59),(1111,'Ostalo',59),(1112,'900',60),(1113,'9000',60),(1114,'9-3',60),(1115,'9-5',60),(1116,'Ostalo',60),(1117,'SL',61),(1118,'Ostalo',61),(1119,'Alhambra',62),(1120,'Altea',62),(1121,'Altea XL',62),(1122,'Arona',62),(1123,'Arosa',62),(1124,'Cordoba',62),(1125,'Exeo',62),(1126,'Ibiza',62),(1127,'Inca',62),(1128,'Leon',62),(1129,'Marbella',62),(1130,'Mii',62),(1131,'Toledo',62),(1132,'Ostalo',62),(1133,'CEO',63),(1134,'Ostalo',63),(1135,'ForFour',64),(1136,'ForTwo',64),(1137,'Roadster',64),(1138,'Ostalo',64),(1139,'Actyon',65),(1140,'Korando',65),(1141,'Kyron',65),(1142,'Musso',65),(1143,'Rexton',65),(1144,'Rodius',65),(1145,'Tivoli',65),(1146,'XLV',65),(1147,'Ostalo',65),(1148,'Forester',66),(1149,'Impreza',66),(1150,'Justy',66),(1151,'Legacy',66),(1152,'Leone',66),(1153,'Libero',66),(1154,'Outback',66),(1155,'Tribeca',66),(1156,'XV',66),(1157,'Ostalo',66),(1158,'Alto',67),(1159,'Baleno',67),(1160,'Grand Vitara',67),(1161,'Ignis',67),(1162,'Jimny',67),(1163,'Kizashi',67),(1164,'Liana',67),(1165,'Maruti',67),(1166,'SJ Samurai',67),(1167,'Splash',67),(1168,'Swift',67),(1169,'SX4',67),(1170,'SX4 S-Cross',67),(1171,'Vitara',67),(1172,'Wagon R+',67),(1173,'Ostalo',67),(1174,'100',68),(1175,'105',68),(1176,'1000 MB',68),(1177,'Citigo',68),(1178,'Fabia',68),(1179,'Favorit',68),(1180,'Felicia',68),(1181,'Karoq',68),(1182,'Kodiaq',68),(1183,'Octavia',68),(1184,'Praktik',68),(1185,'Rapid',68),(1186,'Roomster',68),(1187,'Scala',68),(1188,'Superb',68),(1189,'Yeti',68),(1190,'Ostalo',68),(1191,'Samba',69),(1192,'Ostalo',69),(1193,'Indica',70),(1194,'Safari',70),(1195,'TelcoSport',70),(1196,'Ostalo',70),(1197,'1102',71),(1198,'Ostalo',71),(1199,'4Runner',72),(1200,'Auris',72),(1201,'Avensis',72),(1202,'Avensis Verso',72),(1203,'Aygo',72),(1204,'C-HR',72),(1205,'Camry',72),(1206,'Carina',72),(1207,'Celica',72),(1208,'Corolla',72),(1209,'Corolla Verso',72),(1210,'Hiace',72),(1211,'Hilux',72),(1212,'iQ',72),(1213,'Land Cruiser',72),(1214,'MR2',72),(1215,'Paseo',72),(1216,'Previa',72),(1217,'Prius',72),(1218,'Prius +',72),(1219,'RAV 4',72),(1220,'Sienna',72),(1221,'Starlet',72),(1222,'Tercel',72),(1223,'Urban Cruiser',72),(1224,'Verso',72),(1225,'Yaris',72),(1226,'Yaris Verso',72),(1227,'Ostalo',72),(1228,'601',73),(1229,'Ostalo',73),(1230,'Spitfire',74),(1231,'Ostalo',74),(1232,'469',75),(1233,'31514',75),(1234,'Ostalo',75),(1235,'Vectra',76),(1236,'Ostalo',76),(1237,'181',77),(1238,'Amarok',77),(1239,'Arteon',77),(1240,'Bora',77),(1241,'Buggy',77),(1242,'Buba',77),(1243,'Nova Buba',77),(1244,'Caddy',77),(1245,'Corrado',77),(1246,'Cross Polo',77),(1247,'Derby',77),(1248,'EOS',77),(1249,'Fox',77),(1250,'Golf',77),(1251,'Golf 1',77),(1252,'Golf 2',77),(1253,'Golf 3',77),(1254,'Golf 4',77),(1255,'Golf 5',77),(1256,'Golf 6',77),(1257,'Golf 7',77),(1258,'Golf Plus',77),(1259,'Golf Sportsvan',77),(1260,'Jetta',77),(1261,'Lupo',77),(1262,'Passat',77),(1263,'Passat B1',77),(1264,'Passat B2',77),(1265,'Passat B3',77),(1266,'Passat B4',77),(1267,'Passat B5',77),(1268,'Passat B5.5',77),(1269,'Passat B6',77),(1270,'Passat B7',77),(1271,'Passat B8',77),(1272,'Passat CC',77),(1273,'Phaeton',77),(1274,'Polo',77),(1275,'Scirocco',77),(1276,'Sharan',77),(1277,'T-Roc',77),(1278,'Tiguan',77),(1279,'Touareg',77),(1280,'Touran',77),(1281,'up!',77),(1282,'Vento',77),(1283,'Ostalo',77),(1284,'144',78),(1285,'240',78),(1286,'340',78),(1287,'440',78),(1288,'460',78),(1289,'480',78),(1290,'740',78),(1291,'760',78),(1292,'850',78),(1293,'940',78),(1294,'945',78),(1295,'Amazon',78),(1296,'C30',78),(1297,'C70',78),(1298,'S40',78),(1299,'S60',78),(1300,'S80',78),(1301,'S90',78),(1302,'V40',78),(1303,'V50',78),(1304,'V60',78),(1305,'V70',78),(1306,'V90',78),(1307,'XC60',78),(1308,'XC70',78),(1309,'XC90',78),(1310,'Ostalo',78),(1311,'311',79),(1312,'353',79),(1313,'Ostalo',79),(1314,'10',80),(1315,'101',80),(1316,'128',80),(1317,'1300',80),(1318,'1500',80),(1319,'750',80),(1320,'850',80),(1321,'AR 55',80),(1322,'Florida',80),(1323,'Florida In',80),(1324,'Koral',80),(1325,'Koral In',80),(1326,'Poly',80),(1327,'Skala 55',80),(1328,'Yugo',80),(1329,'Yugo 45',80),(1330,'Yugo 55',80),(1331,'Yugo 60',80),(1332,'Yugo 65',80),(1333,'Yugo Cabrio',80),(1334,'Yugo Ciao',80),(1335,'Yugo In L',80),(1336,'Yugo Tempo',80),(1337,'Ostalo',80),(1338,'ostalo',81),(1362,'KKKK',13),(1363,'sassasas',84),(1364,'NOOOOOOOVI',86),(1365,'sad',86),(1366,'aaabbbbbbnnn',90);
/*!40000 ALTER TABLE `model` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proizvodjac`
--

DROP TABLE IF EXISTS `proizvodjac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `proizvodjac` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv_proizvodjaca` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `naziv_UNIQUE` (`naziv_proizvodjaca`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proizvodjac`
--

LOCK TABLES `proizvodjac` WRITE;
/*!40000 ALTER TABLE `proizvodjac` DISABLE KEYS */;
INSERT INTO `proizvodjac` VALUES (1,'Alfa Romeo'),(2,'Aro'),(3,'Aston Martin'),(4,'Audi'),(5,'Austin'),(6,'Bentley'),(7,'BMW'),(8,'Buick'),(9,'Cadillac'),(10,'Chery'),(11,'Chevrolet'),(12,'Chrysler'),(13,'Citroen'),(14,'Dacia'),(15,'Daewoo'),(16,'Daihatsu'),(17,'Dodge'),(18,'DR'),(19,'Ferrari'),(20,'Fiat'),(21,'Ford'),(22,'GAZ'),(24,'GMC'),(23,'Great Wall'),(25,'Honda'),(26,'Hummer'),(27,'Hyundai'),(28,'Infiniti'),(29,'Isuzu'),(30,'Jaguar'),(31,'Jeep'),(32,'Kia'),(33,'Lada'),(34,'Lamborghini'),(35,'Lancia'),(36,'Land Rover'),(37,'Lexus'),(38,'Lincoln'),(39,'Mahindra'),(40,'Maserati'),(41,'Mazda'),(42,'Mercedes Benz'),(44,'MG'),(43,'Mini'),(45,'Mitsubishi'),(46,'Moszkvics'),(47,'Nissan'),(48,'Oldsmobile'),(49,'Opel'),(81,'Ostalo'),(50,'Peugeot'),(51,'Piaggio'),(52,'Polonez'),(53,'Polski Fiat'),(54,'Pontiac'),(55,'Porsche'),(56,'Proton'),(57,'Renault'),(58,'Rolls Royce'),(59,'Rover'),(60,'Saab'),(61,'Saturn'),(62,'Seat'),(63,'Shuanghuan'),(68,'Škoda'),(64,'Smart'),(65,'SsangYong'),(66,'Subaru'),(67,'Suzuki'),(69,'Talbot'),(70,'Tata'),(71,'Tavria'),(72,'Toyota'),(73,'Trabant'),(74,'Triumph'),(75,'UAZ'),(76,'Vauxhall'),(77,'Volkswagen'),(78,'Volvo'),(79,'Wartburg'),(80,'Zastava');
/*!40000 ALTER TABLE `proizvodjac` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `radninalog`
--

DROP TABLE IF EXISTS `radninalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `radninalog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datum_prijema` datetime NOT NULL,
  `datum_otpusta` datetime DEFAULT NULL,
  `problem` varchar(2000) NOT NULL,
  `Automobil_id` int(11) NOT NULL,
  `ukupna_cena` float DEFAULT NULL,
  `resen_problem` tinyint(1) NOT NULL,
  `Klijent_id` int(11) DEFAULT NULL,
  `opis_intervencija` varchar(2000) DEFAULT NULL,
  `utrosen_materijal` varchar(2000) DEFAULT NULL,
  `cena_materijala` float DEFAULT NULL,
  `nabavna_cena_materijala` float DEFAULT NULL,
  `status` varchar(45) NOT NULL,
  `napomena` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_table2_Automobil1_idx` (`Automobil_id`),
  KEY `fk_RadniNalog_Klijent1_idx` (`Klijent_id`),
  CONSTRAINT `fk_RadniNalog_Klijent1` FOREIGN KEY (`Klijent_id`) REFERENCES `klijent` (`id`),
  CONSTRAINT `fk_table2_Automobil1` FOREIGN KEY (`Automobil_id`) REFERENCES `automobil` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12534 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `radninalog`
--

LOCK TABLES `radninalog` WRITE;
/*!40000 ALTER TABLE `radninalog` DISABLE KEYS */;
INSERT INTO `radninalog` VALUES (12521,'2019-10-10 21:10:28',NULL,'Neispravne kočnice, gašenje motora u vožnji',50,NULL,0,32,NULL,NULL,NULL,NULL,'aktivan',''),(12522,'2019-07-10 21:11:47','2019-09-11 00:09:14','Neispravni amortizeri',52,9600,1,30,'Zamenjeni amortizeri','Neki materijal',2500,2000,'razduzen',''),(12523,'2019-04-08 21:12:24','2019-10-11 00:10:10','Problem sa menjačem',51,16000,1,29,NULL,NULL,0,0,'zatvoren','Klijent je nabavio sve potrebne delove'),(12524,'2019-10-04 21:13:48',NULL,'Neki problem',53,NULL,0,33,NULL,NULL,NULL,NULL,'aktivan','Neka napomena'),(12525,'2019-10-11 00:00:12','2019-10-11 00:13:31','Neispravan motor, istrošene kočnice',54,16400,1,33,NULL,NULL,1400,1200,'zatvoren','Neka napomena za ovaj nalog'),(12526,'2019-10-11 00:01:05',NULL,'Neki problem sa auspuhom',51,NULL,1,31,NULL,NULL,0,0,'aktivan',''),(12527,'2019-10-08 00:02:46',NULL,'Problem sa gašenjam vozila...',56,NULL,0,34,NULL,NULL,NULL,NULL,'otvoren','Klijent je platio deo unapred'),(12528,'2019-10-11 00:19:37',NULL,'Gašenje vozila u vožnji',57,NULL,0,30,NULL,NULL,NULL,NULL,'aktivan',''),(12529,'2019-10-11 00:33:30',NULL,'test problem',55,NULL,0,30,NULL,NULL,NULL,NULL,'aktivan','ostavila kaparu 300e'),(12530,'2019-03-03 00:35:53','2019-08-16 00:58:00','test roblem',54,3000,0,30,'hjsagjdhga',NULL,NULL,NULL,'zatvoren','kapara 300'),(12531,'2019-10-11 15:33:36','2019-10-11 15:36:51','Neki problem sa kocnicama',58,7450,1,29,'Neki opis izvrsenih ingtervencija.',NULL,0,0,'zatvoren','Nsjandaknd adnandajknd'),(12532,'2019-10-17 18:08:19',NULL,'dsabdkabkdnakdnek',58,NULL,0,29,NULL,NULL,NULL,NULL,'aktivan',''),(12533,'2019-10-17 18:14:13',NULL,'JKdhkajdnekjndka',59,NULL,0,34,NULL,NULL,NULL,NULL,'otvoren','');
/*!40000 ALTER TABLE `radninalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `serviser`
--

DROP TABLE IF EXISTS `serviser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `serviser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `korisnicko_ime` varchar(45) NOT NULL,
  `lozinka` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `obrisan` tinyint(1) NOT NULL,
  `pravo` int(11) NOT NULL,
  `ime_servisera` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `prezime_servisera` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `telefon_servisera` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `korisnicko_ime` (`korisnicko_ime`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `serviser`
--

LOCK TABLES `serviser` WRITE;
/*!40000 ALTER TABLE `serviser` DISABLE KEYS */;
INSERT INTO `serviser` VALUES (1,'boban','pbkdf2:sha256:50000$F3lllRzn$247516d2d97ed21c52928a8839851052d8dec4aa18629522f31634fe9c64b0a7',0,1,'Boban','Inđić','062548821'),(2,'momir','pbkdf2:sha256:50000$kbfUQcKf$039559d302f0aad6495443934f3694867c6d464fce9e58e7e8e2feeacdfb58f0',0,2,'Momir','Momirović','06455548821'),(3,'goran','pbkdf2:sha256:50000$0jLFlnFq$eac377359e293be77c118bf3e80862d29dcdaac137b562085eaf668590486fbf',0,2,'Goran','Perić','06588744812'),(33,'sasa','pbkdf2:sha256:50000$B2G1qO0W$aa92fa6f731dc50328260519613f7a0dc57252d8d4c23ff83ab1f820819b4d8c',0,2,'Saša','Sašanović','0651606646416');
/*!40000 ALTER TABLE `serviser` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 16:48:40
