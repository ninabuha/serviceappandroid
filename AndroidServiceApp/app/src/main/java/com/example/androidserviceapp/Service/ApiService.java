package com.example.androidserviceapp.Service;

import android.app.Activity;
import android.net.ParseException;
import android.os.AsyncTask;
import android.text.TextWatcher;
import android.util.Base64;


import com.example.androidserviceapp.Model.Car;
import com.example.androidserviceapp.Model.CarManufacturer;
import com.example.androidserviceapp.Model.CarModel;
import com.example.androidserviceapp.Model.Client;
import com.example.androidserviceapp.Model.PerformedService;
import com.example.androidserviceapp.Model.WorkOrder;
import com.example.androidserviceapp.Model.Repairer;
import com.example.androidserviceapp.Model.Service;
import com.example.androidserviceapp.Viewer.AddWOActivity;
import com.example.androidserviceapp.Viewer.ClientsActivity;
import com.example.androidserviceapp.Viewer.MainActivity;
import com.example.androidserviceapp.Viewer.VehiclesActivity;
import com.example.androidserviceapp.Viewer.WOActivity;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.time.LocalDate;


public class ApiService {

    public static String addWO(String url, final WorkOrder newWorkOrder){

        final String token = MainActivity.getToken();

        AsyncTask<String, Void, String> task = new AsyncTask<String, Void, String>() {
            @Override
            protected void onPreExecute() {
            }

            @Override
            protected String doInBackground(String... strings) {
                String response = "";

                try {
                    URL link = new URL(strings[0]);
                    HttpURLConnection connection = (HttpURLConnection) link.openConnection();

                    connection.setRequestProperty("Content-Type", "application/json");
                    connection.setRequestProperty("x-access-token", token);

                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);
                    connection.setDoOutput(true);

                    JSONObject jsonObject = new JSONObject ();
                    try {
                        jsonObject.put("automobil_id", newWorkOrder.getCar().getId());
                        jsonObject.put("klijent_id", newWorkOrder.getClient().getId());
                        jsonObject.put("problem", newWorkOrder.getProblemDescription());
                        jsonObject.put("napomena", newWorkOrder.getNote());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
                    bw.write(jsonObject.toString());
                    bw.flush();
                    bw.close();

                    connection.getOutputStream().close();
                    connection.connect();

                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                    String row;
                    while ((row = br.readLine())!= null){
                        response += row;
                    }

                    br.close();
                    connection.disconnect();


                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
            }
        };
        task.execute(url);

        try {
            return task.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static void searchWorkOrders(String url, final MainActivity mainActivity, final String searchText) {
        final String token = mainActivity.getToken();

        AsyncTask<String, Void, String> task = new AsyncTask<String, Void, String>() {

            ArrayList<WorkOrder> workOrders = new ArrayList<>();

            @Override
            protected String doInBackground(String... strings) {
                String response = "";

                try {
                    URL link = new URL(strings[0]);
                    HttpURLConnection connection = (HttpURLConnection)link.openConnection();

                    connection.setRequestProperty("Content-Type", "application/json");
                    connection.setRequestProperty("x-access-token", token);

                    connection.setRequestMethod("GET");
                    connection.setDoInput(true);
                    connection.setDoOutput(true);

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    String currentDateandTime = sdf.format(new Date());
                    LocalDate dateFrom = LocalDate.of(1980, 2, 14);

                    JSONObject jsonObject = new JSONObject ();
                    try {
                        jsonObject.put("searchText", searchText);
                        jsonObject.put("fromDate", dateFrom);
                        jsonObject.put("currentDate", currentDateandTime);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
                    bw.write(jsonObject.toString());
                    bw.flush();
                    bw.close();

                    connection.getOutputStream().close();
                    connection.connect();

                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                    String row;
                    while ((row = br.readLine())!= null){
                        response += row + "\n";
                    }

                    try {
                        JSONArray array = new JSONArray(response);
                        Gson gson = new Gson();
                        if (array.length() > 0) {
                            for(int i=0; i<array.length(); i++){
                                JSONObject object = array.getJSONObject(i);

                                CarManufacturer carManufacturer = gson.fromJson(object.toString(), CarManufacturer.class);
                                CarModel carModel = gson.fromJson(object.toString(), CarModel.class);
                                carModel.setCarManufacturer(carManufacturer);
                                Car car = gson.fromJson(object.toString(), Car.class);
                                car.setCarModel(carModel);

                                Client client = gson.fromJson(object.toString(), Client.class);


                                WorkOrder workOrder = new WorkOrder();
                                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
                                LocalDateTime receipt = LocalDateTime.parse(object.get("datum_prijema").toString(), formatter);
                                LocalDateTime release = null;

                                if(!object.isNull("datum_otpusta")){
                                    release = LocalDateTime.parse(object.get("datum_otpusta").toString(), formatter);
                                }

                                workOrder = gson.fromJson(object.toString(), WorkOrder.class);

                                workOrder.setDateOfReceipt(receipt);
                                workOrder.setReleaseDate(release);
                                workOrder.setCar(car);
                                workOrder.setClient(client);

                                workOrders.add(workOrder);
                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    connection.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }


            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                System.out.println("before setwo");
                mainActivity.setWorkOrders(workOrders);
                mainActivity.drawWO();
            }
        };
        task.execute(url);
    }

    public static void searchClients(String url, final ClientsActivity activity) {
        final String token = MainActivity.getToken();

        AsyncTask<String, Void, String> task = new AsyncTask<String, Void, String>() {

            ArrayList<Client> clients = new ArrayList<>();

            @Override
            protected String doInBackground(String... strings) {
                String response = "";

                try {
                    URL link = new URL(strings[0]);
                    HttpURLConnection connection = (HttpURLConnection)link.openConnection();

                    connection.setRequestProperty("Content-Type", "application/json");
                    connection.setRequestProperty("x-access-token", token);


                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                    String row;
                    while ((row = br.readLine())!= null){
                        response += row + "\n";
                    }

                    try {
                        JSONArray array = new JSONArray(response);
                        Gson gson = new Gson();
                        if (array.length() > 0) {
                            for(int i=0; i<array.length(); i++){
                                JSONObject object = array.getJSONObject(i);

                                // getting clients
                                Client client = gson.fromJson(object.toString(), Client.class);
                                clients.add(client);
                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    connection.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }


            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                System.out.println("before setwo");
                activity.setClients(clients);
                activity.drawClients();
            }
        };
        task.execute(url);
    }

    public static void searchVehicles(String url, final VehiclesActivity activity) {
        final String token = MainActivity.getToken();

        AsyncTask<String, Void, String> task = new AsyncTask<String, Void, String>() {

            ArrayList<Car> vehicles = new ArrayList<>();

            @Override
            protected void onPreExecute() {

            }
            @Override
            protected String doInBackground(String... strings) {
                String response = "";

                try {
                    URL link = new URL(strings[0]);
                    HttpURLConnection connection = (HttpURLConnection)link.openConnection();

                    connection.setRequestProperty("Content-Type", "application/json");
                    connection.setRequestProperty("x-access-token", token);

                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                    String row;
                    while ((row = br.readLine())!= null){
                        response += row + "\n";
                    }

                    try {
                        JSONArray array = new JSONArray(response);
                        Gson gson = new Gson();
                        if (array.length() > 0) {
                            for(int i=0; i<array.length(); i++){
                                JSONObject object = array.getJSONObject(i);

                                // getting vehicles
                                CarManufacturer carManufacturer = gson.fromJson(object.toString(), CarManufacturer.class);
                                CarModel carModel = gson.fromJson(object.toString(), CarModel.class);
                                carModel.setCarManufacturer(carManufacturer);
                                Car car = gson.fromJson(object.toString(), Car.class);
                                car.setCarModel(carModel);
                                vehicles.add(car);
                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    connection.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }


            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                System.out.println("before searchVeh");
                System.out.println(vehicles.size());
                activity.setVehicles(vehicles);
                activity.drawVehicles();
            }
        };
        task.execute(url);
    }

    public static String addVehicle(String url, final Car newVehicle){

        final String token = MainActivity.getToken();

        AsyncTask<String, Void, String> task = new AsyncTask<String, Void, String>() {
            @Override
            protected void onPreExecute() {
            }

            @Override
            protected String doInBackground(String... strings) {
                String response = "";

                try {
                    URL link = new URL(strings[0]);
                    HttpURLConnection connection = (HttpURLConnection) link.openConnection();

                    connection.setRequestProperty("Content-Type", "application/json");
                    connection.setRequestProperty("x-access-token", token);

                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);
                    connection.setDoOutput(true);

                    JSONObject jsonObject = new JSONObject ();
                    try {
                        jsonObject.put("model_id", newVehicle.getCarModel().getId());
                        jsonObject.put("broj_sasije", newVehicle.getChassisNumber());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
                    bw.write(jsonObject.toString());
                    bw.flush();
                    bw.close();

                    connection.getOutputStream().close();
                    connection.connect();

                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                    String row;
                    while ((row = br.readLine())!= null){
                        response += row;
                    }

                    br.close();
                    connection.disconnect();


                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
            }
        };
        task.execute(url);

        try {
            return task.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String addClient(String url, final Client newClient){

        final String token = MainActivity.getToken();

        AsyncTask<String, Void, String> task = new AsyncTask<String, Void, String>() {
            @Override
            protected void onPreExecute() {
            }

            @Override
            protected String doInBackground(String... strings) {
                String response = "";

                try {
                    URL link = new URL(strings[0]);
                    HttpURLConnection connection = (HttpURLConnection) link.openConnection();

                    connection.setRequestProperty("Content-Type", "application/json");
                    connection.setRequestProperty("x-access-token", token);

                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);
                    connection.setDoOutput(true);

                    JSONObject jsonObject = new JSONObject ();
                    try {
                        jsonObject.put("ime", newClient.getName());
                        jsonObject.put("prezime", newClient.getLastName());
                        jsonObject.put("telefon", newClient.getPhone());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
                    bw.write(jsonObject.toString());
                    bw.flush();
                    bw.close();

                    connection.getOutputStream().close();
                    connection.connect();

                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                    String row;
                    while ((row = br.readLine())!= null){
                        response += row;
                    }

                    br.close();
                    connection.disconnect();


                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
            }
        };
        task.execute(url);

        try {
            return task.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String editClient(String url, final Client client){

        final String token = MainActivity.getToken();

        AsyncTask<String, Void, String> task = new AsyncTask<String, Void, String>() {
            @Override
            protected void onPreExecute() {
            }

            @Override
            protected String doInBackground(String... strings) {
                String response = "";

                try {
                    URL link = new URL(strings[0]);
                    HttpURLConnection connection = (HttpURLConnection) link.openConnection();

                    connection.setRequestProperty("Content-Type", "application/json");
                    connection.setRequestProperty("x-access-token", token);

                    connection.setRequestMethod("PUT");
                    connection.setDoInput(true);
                    connection.setDoOutput(true);

                    JSONObject jsonObject = new JSONObject ();
                    try {
                        jsonObject.put("ime", client.getName());
                        jsonObject.put("prezime", client.getLastName());
                        jsonObject.put("telefon", client.getPhone());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
                    bw.write(jsonObject.toString());
                    bw.flush();
                    bw.close();

                    connection.getOutputStream().close();
                    connection.connect();

                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                    String row;
                    while ((row = br.readLine())!= null){
                        response += row;
                    }

                    br.close();
                    connection.disconnect();


                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
            }
        };
        task.execute(url);

        try {
            return task.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String addPerformedService(String url, final PerformedService newPerformedSrvice){

        final String token = MainActivity.getToken();

        AsyncTask<String, Void, String> task = new AsyncTask<String, Void, String>() {
            @Override
            protected void onPreExecute() {
            }

            @Override
            protected String doInBackground(String... strings) {
                String response = "";

                try {
                    URL link = new URL(strings[0]);
                    HttpURLConnection connection = (HttpURLConnection) link.openConnection();

                    connection.setRequestProperty("Content-Type", "application/json");
                    connection.setRequestProperty("x-access-token", token);

                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);
                    connection.setDoOutput(true);

                    JSONObject jsonObject = new JSONObject ();
                    try {
                        jsonObject.put("cena_izvrsene_akcije", newPerformedSrvice.getPrice());
                        jsonObject.put("serviser_id", 1);
                        jsonObject.put("akcija_id", newPerformedSrvice.getService().getId());
                        jsonObject.put("radniNalog_id", newPerformedSrvice.getWorkOrder().getId());
                        jsonObject.put("uspesna", newPerformedSrvice.getSuccessful());
                        jsonObject.put("opis_akcije", newPerformedSrvice.getDescription());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
                    bw.write(jsonObject.toString());
                    bw.flush();
                    bw.close();

                    connection.getOutputStream().close();
                    connection.connect();

                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                    String row;
                    while ((row = br.readLine())!= null){
                        response += row;
                    }

                    br.close();
                    connection.disconnect();


                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
            }
        };
        task.execute(url);

        try {
            return task.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String editWOStatus(String url, final WorkOrder wo){

        final String token = MainActivity.getToken();

        AsyncTask<String, Void, String> task = new AsyncTask<String, Void, String>() {
            @Override
            protected void onPreExecute() {
            }

            @Override
            protected String doInBackground(String... strings) {
                String response = "";

                try {
                    URL link = new URL(strings[0]);
                    HttpURLConnection connection = (HttpURLConnection) link.openConnection();

                    connection.setRequestProperty("Content-Type", "application/json");
                    connection.setRequestProperty("x-access-token", token);

                    connection.setRequestMethod("PUT");
                    connection.setDoInput(true);
                    connection.setDoOutput(true);

                    JSONObject jsonObject = new JSONObject ();
                    try {
                        jsonObject.put("ukupna_cena", wo.getTotalPrice());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
                    bw.write(jsonObject.toString());
                    bw.flush();
                    bw.close();

                    connection.getOutputStream().close();
                    connection.connect();

                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                    String row;
                    while ((row = br.readLine())!= null){
                        response += row;
                    }

                    br.close();
                    connection.disconnect();


                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return response;
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
            }
        };
        task.execute(url);

        try {
            return task.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return "";
    }
}

