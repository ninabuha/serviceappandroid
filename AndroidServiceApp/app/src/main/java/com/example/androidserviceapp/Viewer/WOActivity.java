package com.example.androidserviceapp.Viewer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Button;

import com.example.androidserviceapp.Model.Car;
import com.example.androidserviceapp.Model.CarManufacturer;
import com.example.androidserviceapp.Model.CarModel;
import com.example.androidserviceapp.Model.Client;
import com.example.androidserviceapp.Model.Repairer;
import com.example.androidserviceapp.Model.Service;
import com.example.androidserviceapp.R;
import com.example.androidserviceapp.Model.WorkOrder;
import com.example.androidserviceapp.Model.PerformedService;
import com.example.androidserviceapp.Service.AnotherAsyncResponseInterface;
import com.example.androidserviceapp.Service.AnotherMyAsyncTask;
import com.example.androidserviceapp.Service.ApiService;
import com.example.androidserviceapp.Service.AsyncResponseInterface;
import com.example.androidserviceapp.Service.MyAsyncTask;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import android.content.Intent;

public class WOActivity extends AppCompatActivity implements AsyncResponseInterface, AnotherAsyncResponseInterface {

    private MyAsyncTask mt = new MyAsyncTask();
    private AnotherMyAsyncTask anothermt = new AnotherMyAsyncTask();
    private static WorkOrder wo;
    private static ArrayList<PerformedService> performedServices;

    private ImageView woIcon;
    private TextView woNumber, status, dateOpened, dateClosed, car, chassisNumber, client, phone, problemDescription, note, ps, totalPrice, totalPriceText;

    private SharedPreferences sharedPreferences;
    private String address;

    private Button backBtn, addServiceBtn, closeWOBtn;

    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_order);

        mt.delegate = this;
        anothermt.delegate = this;

        // ip address
        getAddress();

        initWO();
        performedServices = new ArrayList<>();
        initPerformedServices();


        // setting back button
        backBtn = findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(WOActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public void getAddress(){
        sharedPreferences = getSharedPreferences("MySharedPreferences", 0);
        String address = sharedPreferences.getString("address", "");
        this.address = address;
    }
    public static WorkOrder getWo(){
        return wo;
    }

    // setting text for performed services
    public String drawPS(ArrayList<PerformedService> performedServices){
        String services = "";
        int counter = 0;
        for(PerformedService ps: performedServices){
            if(counter != 0){
                services += ", ";
            }
            services += ps.getService().getName();
            counter+=1;
        }
        ps.setText(services);
        return services;
    }

    private void initPerformedServices() {
        mt = new MyAsyncTask();
        mt.delegate = WOActivity.this;
        mt.execute(address + "/izvrseneAkcije/" + MainActivity.getTemporaryWO().getId());
    }
    private void initWO(){
        anothermt = new AnotherMyAsyncTask();
        anothermt.delegate = WOActivity.this;
        anothermt.execute(address + "/radniNalozi/" + MainActivity.getTemporaryWO().getId());
    }
    public static void setPerformedServices(ArrayList<PerformedService> ps){
        performedServices = ps;
    }

    public void drawWO(){
        // finding fields by id
        woIcon = findViewById(R.id.woIcon);
        woNumber = findViewById(R.id.woNumber);
        status = findViewById(R.id.status);
        dateOpened = findViewById(R.id.dateOpened);
        dateClosed = findViewById(R.id.dateClosed);
        car = findViewById(R.id.car);
        chassisNumber = findViewById(R.id.chassisNumber);
        client = findViewById(R.id.client);
        phone = findViewById(R.id.phone);
        problemDescription = findViewById(R.id.problemDescription);
        note = findViewById(R.id.note);
        ps = findViewById(R.id.performedServices);

        // populating fields
        woIcon.setImageResource(R.drawable.wo_icon);
        woNumber.setText("Radni Nalog #" + wo.getId());
        status.setText(wo.getStatus());
        dateOpened.setText(wo.getDateOfReceipt().toString());
        if(wo.getReleaseDate() != null){
            System.out.println(wo.getReleaseDate().toString());
            dateClosed.setText(wo.getReleaseDate().toString());
        }
        car.setText(wo.getCar().getCarModel().getCarManufacturer().getName() + " " + wo.getCar().getCarModel().getName());
        chassisNumber.setText(wo.getCar().getChassisNumber());
        client.setText(wo.getClient().getName() + " " + wo.getClient().getLastName());
        phone.setText(wo.getClient().getPhone());
        problemDescription.setText(wo.getProblemDescription().toString());
        if(wo.getNote() != null){
            note.setText(wo.getNote());
        }
    }

    @Override
    public void onTaskCompleted(String output) {
        try {
            JSONArray array = new JSONArray(output);
            Gson gson = new Gson();
            if (array.length() > 0) {
                for(int i=0; i<array.length(); i++){
                    JSONObject object = array.getJSONObject(i);

                    // making ps object with gson
                    PerformedService ps = gson.fromJson(object.toString(), PerformedService.class);
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
                    LocalDateTime date = LocalDateTime.parse(object.get("datum_izvrsavanja").toString(), formatter);

                    // making service, wo and repairer objects
                    Service service = gson.fromJson(object.toString(), Service.class);
                    WorkOrder wo = gson.fromJson(object.toString(), WorkOrder.class);
                    Repairer repairer = gson.fromJson(object.toString(), Repairer.class);

                    // setting those objects
                    ps.setDate(date);
                    ps.setService(service);
                    ps.setWorkOrder(wo);
                    ps.setRepairer(repairer);

                    performedServices.add(ps);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(performedServices.size() > 0){
            drawPS(performedServices);
        }
    }

    @Override
    public void anotherOnTaskCompleted(String output) {
        try {
            JSONObject object = new JSONObject(output);
            Gson gson = new Gson();
            if (output != "") {
                CarManufacturer carManufacturer = gson.fromJson(object.toString(), CarManufacturer.class);
                CarModel carModel = gson.fromJson(object.toString(), CarModel.class);
                carModel.setCarManufacturer(carManufacturer);
                Car car = gson.fromJson(object.toString(), Car.class);
                car.setCarModel(carModel);

                Client client = gson.fromJson(object.toString(), Client.class);

                WorkOrder workOrder = new WorkOrder();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
                LocalDateTime receipt = LocalDateTime.parse(object.get("datum_prijema").toString(), formatter);

                // making new wo with gson
                workOrder = gson.fromJson(object.toString(), WorkOrder.class);
                LocalDateTime release = null;

                if(!object.isNull("datum_otpusta")){
                    release = LocalDateTime.parse(object.get("datum_otpusta").toString(), formatter);
                }


                workOrder.setDateOfReceipt(receipt);
                workOrder.setReleaseDate(release);
                workOrder.setCar(car);
                workOrder.setClient(client);

                wo = workOrder;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        drawWO();
        // if wo not closed
        if(!wo.getStatus().equals("zatvoren")){
            drawCloseAndAddBtns();
        }
        else{
            drawTotalPrice();
        }

    }


    private void drawCloseAndAddBtns() {

        // making btns visible
        closeWOBtn = findViewById(R.id.closeWoBtn);
        closeWOBtn.setVisibility(View.VISIBLE);
        closeWOBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float totalPrice = 0.0f;
                for(int i = 0; i < performedServices.size(); i++){
                    totalPrice += performedServices.get(i).getPrice();
                }
                wo.setTotalPrice(totalPrice);
                ApiService.editWOStatus(address + "/radniNalozi/zatvaranje/" + wo.getId(), wo);
                // refreshing activity
                finish();
                startActivity(getIntent());
            }
        });

        addServiceBtn = findViewById(R.id.addServiceBtn);
        addServiceBtn.setVisibility(View.VISIBLE);
        addServiceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(WOActivity.this, AddPerformedServiceActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void drawTotalPrice() {
        // making total price visible
        totalPriceText = findViewById(R.id.totalPriceText);
        totalPriceText.setVisibility(View.VISIBLE);

        totalPrice = findViewById(R.id.totalPrice);
        totalPrice.setVisibility(View.VISIBLE);
        totalPrice.setText(Float.toString(wo.getTotalPrice()));
    }
}
