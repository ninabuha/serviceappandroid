package com.example.androidserviceapp.Viewer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.androidserviceapp.Model.Service;
import com.example.androidserviceapp.Model.PerformedService;
import com.example.androidserviceapp.Model.WorkOrder;
import com.example.androidserviceapp.R;
import android.widget.Spinner;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Toast;

import com.example.androidserviceapp.Service.ApiService;
import com.example.androidserviceapp.Service.AsyncResponseInterface;
import com.example.androidserviceapp.Service.MyAsyncTask;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AddPerformedServiceActivity extends AppCompatActivity implements AsyncResponseInterface {

    private MyAsyncTask mt;

    private WorkOrder wo;
    private ArrayList<Service> services;
    private Service service;

    private SharedPreferences sharedPreferences;
    private String address;

    private Button backBtn, addSelectedServiceBtn;

    private Intent intent;

    private Spinner servicesSpinner;

    private TextView woId;
    private EditText searchService, price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_performed_service);

        // ip address
        getAddress();

        wo = WOActivity.getWo();

        mt = new MyAsyncTask();
        mt.delegate = this;

        // getting services
        initServices();

        woId = findViewById(R.id.wOId);
        woId.setText("Work Order #" + wo.getId());

        backBtn = findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        searchService = findViewById(R.id.searchService);
        searchService.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(searchService.getText().toString().equals("")){
                    initServices();
                }
                else{
                    ArrayList<Service> matchServices = new ArrayList<>();
                    for(Service ser : services){
                        // checking if search text matches service name
                        if(ser.getName().toLowerCase().contains(searchService.getText().toString().toLowerCase())){
                            matchServices.add(ser);
                        }
                    }
                    services = matchServices;
                    fillServicesSpinner();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        servicesSpinner = findViewById(R.id.serviceSpinner);
        servicesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // getting a service object at selected position
                service = services.get(pos);
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        price = findViewById(R.id.price);

        addSelectedServiceBtn = findViewById(R.id.addSelectedServiceBtn);
        addSelectedServiceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(service != null && !price.getText().toString().equals("")){
                    PerformedService newPerformedService = new PerformedService();
                    newPerformedService.setPrice(Integer.parseInt(price.getText().toString()));
                    newPerformedService.setSuccessful(1);
                    newPerformedService.setService(service);
                    newPerformedService.setWorkOrder(wo);
                    newPerformedService.setDescription("");
                    ApiService.addPerformedService(address + "/izvrseneAkcije", newPerformedService);
                    if(wo.getStatus().equals("otvoren")){
                        ApiService.editWOStatus(address + "/radniNalozi/aktivan/" + wo.getId(), wo);
                    }
                    intent = new Intent(AddPerformedServiceActivity.this, WOActivity.class);
                    startActivity(intent);
                    finish();
                }
                else{
                    Toast.makeText(AddPerformedServiceActivity.this, "Neophodno je uneti cenu!", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private void initServices() {
        services = new ArrayList<>();
        mt = new MyAsyncTask();
        mt.delegate = this;
        mt.execute(address + "/akcije");
    }

    public void getAddress(){
        sharedPreferences = getSharedPreferences("MySharedPreferences", 0);
        String address = sharedPreferences.getString("address", "");
        this.address = address;
    }

    @Override
    public void onTaskCompleted(String output) {
        services = new ArrayList<>();
        try {
            JSONArray array = new JSONArray(output);
            System.out.println(array);
            Gson gson = new Gson();
            if (array.length() > 0) {
                for(int i=0; i<array.length(); i++){
                    JSONObject object = array.getJSONObject(i);

                    // getting service
                    Service service = gson.fromJson(object.toString(), Service.class);
                    services.add(service);
                }
                System.out.println(services);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        fillServicesSpinner();
    }

    private void fillServicesSpinner() {
        servicesSpinner = findViewById(R.id.serviceSpinner);
        ArrayAdapter<Service> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, services);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        servicesSpinner.setAdapter(adapter);
    }
}
