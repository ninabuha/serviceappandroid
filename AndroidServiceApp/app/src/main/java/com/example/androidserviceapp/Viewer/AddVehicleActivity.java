package com.example.androidserviceapp.Viewer;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.androidserviceapp.Model.Client;
import com.example.androidserviceapp.Model.WorkOrder;
import com.example.androidserviceapp.R;
import com.example.androidserviceapp.Service.AnotherAsyncResponseInterface;
import com.example.androidserviceapp.Service.AnotherMyAsyncTask;
import com.example.androidserviceapp.Service.ApiService;
import com.example.androidserviceapp.Service.AsyncResponseInterface;
import com.example.androidserviceapp.Service.MyAsyncTask;

import java.util.ArrayList;

import com.example.androidserviceapp.Model.Car;
import com.example.androidserviceapp.Model.CarManufacturer;
import com.example.androidserviceapp.Model.CarModel;
import com.google.gson.Gson;

import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AddVehicleActivity extends AppCompatActivity implements AsyncResponseInterface, AnotherAsyncResponseInterface {

    private MyAsyncTask mt = new MyAsyncTask();
    private AnotherMyAsyncTask anothermt = new AnotherMyAsyncTask();

    private static ArrayList<CarManufacturer> manufacturers;
    private static ArrayList<CarModel> models;

    private Button backBtn, addBtn;

    private Spinner manufacturerSpinner, modelSpinner;
    private EditText chassisNumber;

    private Intent intent;

    private CarManufacturer carManufacturer;
    private CarModel carModel;

    private SharedPreferences sharedPreferences;
    private String address;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vehicle);

        mt.delegate = this;
        anothermt.delegate = this;

        geAddress();

        manufacturers = new ArrayList<>();
        models = new ArrayList<>();

        initManufacturers();

        manufacturerSpinner = findViewById(R.id.manufacturerSpinner);
        manufacturerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                carManufacturer = manufacturers.get(position);
                initModels();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        modelSpinner = findViewById(R.id.modelSpinner);
        modelSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                carModel = models.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // back btn
        backBtn = findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // add btn
        addBtn = findViewById(R.id.addBtn);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chassisNumber = findViewById(R.id.chassisNumber);

                if(chassisNumber.getText().toString().equals("")){
                    Toast.makeText(AddVehicleActivity.this, "Chassis number can not be empty", Toast.LENGTH_LONG).show();
                }
                else {
                    Car newCar = new Car();
                    newCar.setCarModel(carModel);
                    newCar.setChassisNumber(chassisNumber.getText().toString());
                    ApiService.addVehicle(address + "/automobili", newCar);
                    Toast.makeText(AddVehicleActivity.this, "Vehicle added successfully", Toast.LENGTH_LONG).show();
                    intent = new Intent(AddVehicleActivity.this, VehiclesActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    private void initManufacturers() {
        mt.execute(address + "/proizvodjaci");
    }
    private void initModels(){
        anothermt = new AnotherMyAsyncTask();
        anothermt.delegate = this;
        anothermt.execute(address + "/modeli/" + carManufacturer.getId());
    }

    private void geAddress() {
        sharedPreferences = getSharedPreferences("MySharedPreferences", 0);
        String address = sharedPreferences.getString("address", "");
        this.address = address;
    }

    @Override
    public void onTaskCompleted(String output) {
        try {
            JSONArray array = new JSONArray(output);
            Gson gson = new Gson();
            if (array.length() > 0) {
                for(int i=0; i<array.length(); i++){
                    JSONObject object = array.getJSONObject(i);

                    // getting car manufacturer
                    CarManufacturer carManufacturer = gson.fromJson(object.toString(), CarManufacturer.class);
                    manufacturers.add(carManufacturer);
                }

            }
            fillManufacturerSpinner();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void fillManufacturerSpinner() {
        ArrayAdapter<CarManufacturer> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, manufacturers);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        manufacturerSpinner.setAdapter(adapter);
    }

    @Override
    public void anotherOnTaskCompleted(String output) {
        models = new ArrayList<>();
        try {
            JSONArray array = new JSONArray(output);
            Gson gson = new Gson();
            if (array.length() > 0) {
                for(int i=0; i<array.length(); i++){
                    JSONObject object = array.getJSONObject(i);

                    // getting car models
                    CarModel carModel = gson.fromJson(object.toString(), CarModel.class);
                    models.add(carModel);
                }

            }
            fillModelSpinner();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void fillModelSpinner() {
        ArrayAdapter<CarModel> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, models);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        modelSpinner.setAdapter(adapter);
    }
}
