package com.example.androidserviceapp.Model;

import com.google.gson.annotations.SerializedName;

public class Client {

    private int id;
    @SerializedName("ime")
    private String name;
    @SerializedName("prezime")
    private String lastName;
    @SerializedName("telefon")
    private String phone;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public Client(){

    }
    public  Client(int id, String name, String lastName, String phone){
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.phone = phone;
    }

    @Override
    public String toString() {
        return name + " " + lastName + " " + phone;
    }
}
