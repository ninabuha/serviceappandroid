package com.example.androidserviceapp.Viewer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;

import com.example.androidserviceapp.R;
import com.example.androidserviceapp.Model.Client;
import com.example.androidserviceapp.Service.ApiService;

import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;

public class ClientActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private String address;

    private Client client;
    EditText name, lastName, phone;
    Button editBtn, backBtn;

    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);

        // ip address
        getAddress();


        client = ClientsActivity.getTemporaryClient();

        name = findViewById(R.id.clientName);
        lastName = findViewById(R.id.clientLastName);
        phone = findViewById(R.id.clientPhone);
        name.setText(client.getName());
        lastName.setText(client.getLastName());
        phone.setText(client.getPhone());

        editBtn = findViewById(R.id.editBtn);
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                client.setName(name.getText().toString());
                client.setLastName(lastName.getText().toString());
                client.setPhone(phone.getText().toString());
                ApiService.editClient(address + "/klijenti/" + client.getId(), client);
                i = new Intent(ClientActivity.this, ClientsActivity.class);
                startActivity(i);
            }
        });

        // setting back button
        backBtn = findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void getAddress(){
        sharedPreferences = getSharedPreferences("MySharedPreferences", 0);
        String address = sharedPreferences.getString("address", "");
        this.address = address;
    }
}
