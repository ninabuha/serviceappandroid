package com.example.androidserviceapp.Service;
import android.os.AsyncTask;

import com.example.androidserviceapp.Viewer.MainActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MyAsyncTask extends AsyncTask<String, Void, String> {
    public AsyncResponseInterface delegate = null;

    String token = null;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // getting token from shared preferences
        token = MainActivity.getToken();
    }

    @Override
    protected String doInBackground(String... strings) {
        String response = "";

        try {
            URL link = new URL(strings[0]);
            HttpURLConnection connection = (HttpURLConnection)link.openConnection();

            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("x-access-token", token);

            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String row;
            while ((row = br.readLine())!= null){
                response += row + "\n";
            }

            connection.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    protected void onPostExecute(String result) {
        delegate.onTaskCompleted(result);
    }
}
