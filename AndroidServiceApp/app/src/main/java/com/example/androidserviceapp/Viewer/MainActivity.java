package com.example.androidserviceapp.Viewer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.EditText;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.inputmethod.InputMethodManager;

import com.example.androidserviceapp.Model.Car;
import com.example.androidserviceapp.Model.CarManufacturer;
import com.example.androidserviceapp.Model.CarModel;
import com.example.androidserviceapp.Model.Client;
import com.example.androidserviceapp.Service.ApiService;
import com.example.androidserviceapp.Model.WorkOrder;
import com.example.androidserviceapp.R;
import com.example.androidserviceapp.Service.AsyncResponseInterface;
import com.example.androidserviceapp.Service.MyAsyncTask;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements AsyncResponseInterface {

    private MyAsyncTask mt = new MyAsyncTask();
    private static String token;

    private static ArrayList<WorkOrder> workOrders;

    private Button wOMenuBtn, clientsMenuBtn, vehiclesMenuBtn, addBtn, viewBtn;
    private LinearLayout mainLayout, wOLayout;

    private ArrayList<LinearLayout> listWOLayout;

    private static WorkOrder temporaryWO;


    private Intent intent;
    private SharedPreferences sharedPreferences;
    private String address;

    private EditText searchField;
    private ImageButton searchBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mt.delegate = this;

        // ip address
        getAddress();

        // token
        getTokenFromSharedPreferences();

        // getting wos
        workOrders = new ArrayList<>();
        initWorkOrders();

        // changing active menu btn
        wOMenuBtn = findViewById(R.id.wwOMenuBtn);
        wOMenuBtn.setBackgroundColor(Color.argb(255, 255, 255, 255));

        clientsMenuBtn = findViewById(R.id.wclientsBtn);
        clientsMenuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, ClientsActivity.class);
                startActivity(intent);
            }
        });

        vehiclesMenuBtn = findViewById(R.id.wvehiclesBtn);
        vehiclesMenuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, VehiclesActivity.class);
                startActivity(intent);
            }
        });


        // instantiating LinearLayout array
        listWOLayout = new ArrayList<>();

        final MainActivity activity = this;

        searchField = findViewById(R.id.searchWOField);

        // mozda ne ovako
        // listener on search box
        /*searchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ApiService.searchWorkOrders( address + "/radniNalozi/pretraga", activity, searchField.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });*/

        // nego ovako
        // listener on search btn
        searchBtn = findViewById(R.id.searchWOBtn);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApiService.searchWorkOrders( address + "/radniNalozi/pretraga", activity, searchField.getText().toString());
                // hiding keyboard on btn click
                try {
                    InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        });


        // listener on addBtn
        addBtn = findViewById(R.id.addWOBtn);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, AddWOActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("RESUME");
        initWorkOrders();
    }

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("START");
        initWorkOrders();
    }

    public void getAddress(){
        sharedPreferences = getSharedPreferences("MySharedPreferences", 0);
        String address = sharedPreferences.getString("address", "");
        this.address = address;
    }

    public void initWorkOrders(){
        mt = new MyAsyncTask();
        mt.delegate = this;
        mt.execute(address + "/radniNalozi");
    }

    public static ArrayList<WorkOrder> getWorkOrders(){
        return workOrders;
    }
    public static void setWorkOrders(ArrayList<WorkOrder> wO){
        workOrders = wO;
    }


    public void getTokenFromSharedPreferences(){
        SharedPreferences sharedPreferences = getSharedPreferences("MySharedPreferences", 0);
        String token = sharedPreferences.getString("token", "");
        this.token = token;
    }
    public static String getToken(){
        return token;
    }

    public void drawWO(){
        System.out.println("draw wo");

        LayoutInflater inflater = LayoutInflater.from(this);
        mainLayout = findViewById(R.id.mainLayout);
        mainLayout.removeAllViews();

        // car and client icons
        ImageView carIcon;
        ImageView clientIcon;

        // making view components for each wo
        TextView wOIdView;
        TextView wOStatusView;
        TextView receivedDateView;
        TextView carManufacturerView, carModelView;
        TextView clientNameView, clientLastNameView, clientTel;

        String woId;
        String woStatus;
        String woReceivedDate;
        CarManufacturer woCarManufacturer;
        CarModel woCarModel;
        Client woClient;

        int counter = 0;

        for(final WorkOrder wo: workOrders){

            // setting a temporary wo
            temporaryWO = wo;

            // filling mainLayout with wOLayouts
            wOLayout = (LinearLayout) inflater.inflate(R.layout.work_order, mainLayout, false);
            listWOLayout.add(wOLayout);

            // setting icons
            carIcon = wOLayout.findViewById(R.id.carIcon);
            carIcon.setImageResource(R.drawable.car_icon);
            clientIcon = wOLayout.findViewById(R.id.clientIcon);
            clientIcon.setImageResource(R.drawable.client_icon);

            // getting data from work order
            woId = Integer.toString(wo.getId());
            woStatus = wo.getStatus();
            woReceivedDate = wo.getDateOfReceipt().toString();
            woCarManufacturer = wo.getCar().getCarModel().getCarManufacturer();
            woCarModel = wo.getCar().getCarModel();
            woClient = wo.getClient();

            wOIdView = wOLayout.findViewById(R.id.wOId);
            wOStatusView = wOLayout.findViewById(R.id.woStatus);
            receivedDateView = wOLayout.findViewById(R.id.receivedDate);
            carManufacturerView = wOLayout.findViewById(R.id.carManufacturer);
            carModelView = wOLayout.findViewById(R.id.carModel);
            clientNameView = wOLayout.findViewById(R.id.clientName);
            clientLastNameView = wOLayout.findViewById(R.id.clientLastName);
            clientTel = wOLayout.findViewById(R.id.clientTel);

            // drawing data to view components
            wOIdView.setText("#" + woId);
            if(woStatus.equals("zatvoren")){
                wOStatusView.setTextColor(Color.RED);
            }
            else if(woStatus.equals("aktivan")){
                wOStatusView.setTextColor(Color.BLACK);
            }
            wOStatusView.setText(woStatus);
            receivedDateView.setText(woReceivedDate);
            carManufacturerView.setText(woCarManufacturer.getName().toString());
            carModelView.setText(woCarModel.getName().toString());
            clientNameView.setText(woClient.getName().toString());
            clientLastNameView.setText(woClient.getLastName().toString());
            clientTel.setText(woClient.getPhone().toString());

            counter +=1;

            // listener on viewBtn
            viewBtn = wOLayout.findViewById(R.id.viewBtn);
            viewBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // setting a temporary wo
                    setTemporaryWO(wo);
                    intent = new Intent(MainActivity.this, WOActivity.class);
                    startActivity(intent);
                }
            });

            // changing margins for last wo layout because of add btn
            if(workOrders.size() == counter){
                ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) wOLayout.getLayoutParams();
                p.setMargins(30, 60, 30, 150);
                wOLayout.requestLayout();
            }

            mainLayout.addView(wOLayout);
        }
    }

    public void setTemporaryWO(WorkOrder wo){
        temporaryWO = wo;
    }
    public static WorkOrder getTemporaryWO(){
        return temporaryWO;
    }

    @Override
    public void onTaskCompleted(String output) {
        workOrders = new ArrayList<>();
        try {
            JSONArray array = new JSONArray(output);
            Gson gson = new Gson();
            if (array.length() > 0) {
                for(int i=0; i<array.length(); i++){
                    JSONObject object = array.getJSONObject(i);

                    CarManufacturer carManufacturer = gson.fromJson(object.toString(), CarManufacturer.class);
                    CarModel carModel = gson.fromJson(object.toString(), CarModel.class);
                    carModel.setCarManufacturer(carManufacturer);
                    Car car = gson.fromJson(object.toString(), Car.class);
                    car.setCarModel(carModel);

                    Client client = gson.fromJson(object.toString(), Client.class);


                    WorkOrder workOrder = new WorkOrder();
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
                    LocalDateTime receipt = LocalDateTime.parse(object.get("datum_prijema").toString(), formatter);

                    // making new wo with gson
                    workOrder = gson.fromJson(object.toString(), WorkOrder.class);
                    LocalDateTime release = null;

                    if(!object.isNull("datum_otpusta")){
                        release = LocalDateTime.parse(object.get("datum_otpusta").toString(), formatter);
                    }


                    workOrder.setDateOfReceipt(receipt);
                    workOrder.setReleaseDate(release);
                    workOrder.setCar(car);
                    workOrder.setClient(client);

                    workOrders.add(workOrder);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        // drawing wo
        drawWO();
    }
}
