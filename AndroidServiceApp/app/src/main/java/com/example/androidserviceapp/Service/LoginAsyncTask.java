package com.example.androidserviceapp.Service;

import android.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class LoginAsyncTask extends AsyncTask<String, Void, String> {
    public AsyncResponseInterface delegate = null;
    public String username = "";
    public String password = "";

    @Override
    protected String doInBackground(String... strings) {
        String response = "";

        try {
            URL link = new URL(strings[0]);
            HttpURLConnection connection = (HttpURLConnection) link.openConnection();

            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);

            JSONObject jsonObject = new JSONObject ();
            try {
                jsonObject.put("username", Base64.encodeToString(username.getBytes(), Base64.NO_WRAP));
                jsonObject.put("password", Base64.encodeToString(password.getBytes(), Base64.NO_WRAP));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
            bw.write(jsonObject.toString());
            bw.flush();
            bw.close();

            connection.getOutputStream().close();
            connection.connect();

            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String row;
            while ((row = br.readLine())!= null){
                response += row;
            }

            br.close();
            connection.disconnect();


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    protected void onPostExecute(String result) {
        delegate.onTaskCompleted(result);
    }
}
