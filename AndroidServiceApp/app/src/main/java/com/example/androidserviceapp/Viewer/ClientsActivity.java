package com.example.androidserviceapp.Viewer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.EditText;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.inputmethod.InputMethodManager;
import android.os.Bundle;
import com.example.androidserviceapp.Model.Client;

import com.example.androidserviceapp.Model.WorkOrder;
import com.example.androidserviceapp.R;
import com.example.androidserviceapp.Service.AnotherAsyncResponseInterface;
import com.example.androidserviceapp.Service.AnotherMyAsyncTask;
import com.example.androidserviceapp.Service.ApiService;
import com.example.androidserviceapp.Service.AsyncResponseInterface;
import com.example.androidserviceapp.Service.MyAsyncTask;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ClientsActivity extends AppCompatActivity implements AsyncResponseInterface {

    private MyAsyncTask mt = new MyAsyncTask();
    private static String token;

    private static ArrayList<Client> clients;
    private Button wOMenuBtn, clientsMenuBtn, vehiclesMenuBtn, addBtn, viewBtn;

    private LinearLayout clientsLayout, cLayout;

    private ArrayList<LinearLayout> listClientLayout;

    private static Client temporaryClient;

    private Intent intent;
    private SharedPreferences sharedPreferences;
    private String address;

    private EditText searchField;
    private ImageButton searchBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clients);

        mt.delegate = this;
        final ClientsActivity activity = this;

        // ip address
        getAddress();
        token = MainActivity.getToken();

        clients = new ArrayList<>();
        initClients();

        listClientLayout = new ArrayList<>();

        // changing active menu btn
        clientsMenuBtn = findViewById(R.id.cclientsBtn);
        clientsMenuBtn.setBackgroundColor(Color.argb(255, 255, 255, 255));

        wOMenuBtn = findViewById(R.id.cwOMenuBtn);
        wOMenuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ClientsActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        vehiclesMenuBtn = findViewById(R.id.cvehiclesBtn);
        vehiclesMenuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ClientsActivity.this, VehiclesActivity.class);
                startActivityForResult(intent, 2);
            }
        });

        searchField = findViewById(R.id.searchCField);
        searchBtn = findViewById(R.id.searchCBtn);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(searchField.getText().toString().equals("")){
                    mt = new MyAsyncTask();
                    mt.delegate = activity;
                    mt.execute(address + "/klijenti");
                }
                else{
                    ApiService.searchClients( address + "/klijenti" + "/" + searchField.getText().toString() + "/", activity);
                }

                // hiding keyboard on btn click
                try {
                    InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        });

        // listener on addBtn
        addBtn = findViewById(R.id.addCBtn);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(ClientsActivity.this, AddClientActivity.class);
                startActivity(intent);
            }
        });
    }

    public void getAddress(){
        sharedPreferences = getSharedPreferences("MySharedPreferences", 0);
        String address = sharedPreferences.getString("address", "");
        this.address = address;
    }

    public void initClients() {
        mt.execute(address + "/klijenti");
    }
    public void setClients(ArrayList<Client> cls){
        clients = cls;
    }

    public void drawClients(){
        System.out.println("draw clients");

        LayoutInflater inflater = LayoutInflater.from(this);
        clientsLayout = findViewById(R.id.clientsLayout);
        clientsLayout.removeAllViews();

        ImageView clientIcon;
        TextView clinetNameLastName;
        TextView clientPhone;

        int counter = 0;

        for(final Client c: clients){
            // setting a temporary wo
            temporaryClient = c;

            // filling mainLayout with wOLayouts
            cLayout = (LinearLayout) inflater.inflate(R.layout.client, clientsLayout, false);
            listClientLayout.add(cLayout);

            // setting icon
            clientIcon = cLayout.findViewById(R.id.clientIcon);
            clientIcon.setImageResource(R.drawable.client_icon);

            // getting data from work order
            clinetNameLastName = cLayout.findViewById(R.id.client);
            clientPhone = cLayout.findViewById(R.id.phone);

            // drawing data to view components
            clinetNameLastName.setText(c.getName() + " " + c.getLastName());
            clientPhone.setText(c.getPhone());

            viewBtn = cLayout.findViewById(R.id.viewBtn);
            viewBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setTemporaryClient(c);
                    intent = new Intent(ClientsActivity.this, ClientActivity.class);
                    startActivity(intent);
                }
            });

            counter +=1;

            // changing margins for last wo layout because of add btn
            if(clients.size() == counter){
                ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) cLayout.getLayoutParams();
                p.setMargins(0, 0, 0, 150);
                cLayout.requestLayout();
            }

            clientsLayout.addView(cLayout);
        }
    }

    public void setTemporaryClient(Client c){
        temporaryClient = c;
    }
    public static Client getTemporaryClient(){
        return temporaryClient;
    }

    @Override
    public void onTaskCompleted(String output) {
        clients = new ArrayList<>();
        try {
            JSONArray array = new JSONArray(output);
            System.out.println(array);
            Gson gson = new Gson();
            if (array.length() > 0) {
                for(int i=0; i<array.length(); i++){
                    JSONObject object = array.getJSONObject(i);

                    // getting clients
                    Client client = gson.fromJson(object.toString(), Client.class);
                    clients.add(client);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        drawClients();
    }

}
