package com.example.androidserviceapp.Model;


import com.example.androidserviceapp.Model.Car;
import com.example.androidserviceapp.Model.Client;
import com.google.gson.annotations.SerializedName;

import java.time.LocalDateTime;


public class WorkOrder {



    private int id;
    private Car car;
    private Client client;
    private LocalDateTime dateOfReceipt;
    private LocalDateTime releaseDate;
    @SerializedName("problem")
    private String problemDescription;
    @SerializedName("opis_intervencija")
    private String serviceDescription;
    @SerializedName("utrosen_materijal")
    private String usedMaterijal;
    private String status;
    @SerializedName("napomena")
    private String note;
    @SerializedName("ukupna_cena")
    private float totalPrice;
    @SerializedName("cena_materijala")
    private float materialPrice;
    @SerializedName("nabavna_cena_materijala")
    private float materialPurchasePrice;
    private boolean problemSolved;



    public WorkOrder(){

    }
    public WorkOrder(int id, Car car, Client client, LocalDateTime dateOfReceipt, LocalDateTime releaseDate, String problemDescription, String serviceDescription, String usedMaterijal, String status, String note, float totalPrice, float materialPrice, float materialPurchasePrice, boolean problemSolved) {
        this.id = id;
        this.car = car;
        this.client = client;
        this.dateOfReceipt = dateOfReceipt;
        this.releaseDate = releaseDate;
        this.problemDescription = problemDescription;
        this.serviceDescription = serviceDescription;
        this.usedMaterijal = usedMaterijal;
        this.status = status;
        this.note = note;
        this.totalPrice = totalPrice;
        this.materialPrice = materialPrice;
        this.materialPurchasePrice = materialPurchasePrice;
        this.problemSolved = problemSolved;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public LocalDateTime getDateOfReceipt() {
        return dateOfReceipt;
    }

    public void setDateOfReceipt(LocalDateTime dateOfReceipt) {
        this.dateOfReceipt = dateOfReceipt;
    }

    public LocalDateTime getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDateTime releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getProblemDescription() {
        return problemDescription;
    }

    public void setProblemDescription(String problemDescription) {
        this.problemDescription = problemDescription;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public String getUsedMaterijal() {
        return usedMaterijal;
    }

    public void setUsedMaterijal(String usedMaterijal) {
        this.usedMaterijal = usedMaterijal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public float getMaterialPrice() {
        return materialPrice;
    }

    public void setMaterialPrice(float materialPrice) {
        this.materialPrice = materialPrice;
    }

    public float getMaterialPurchasePrice() {
        return materialPurchasePrice;
    }

    public void setMaterialPurchasePrice(float materialPurchasePrice) {
        this.materialPurchasePrice = materialPurchasePrice;
    }

    public boolean isProblemSolved() {
        return problemSolved;
    }

    public void setProblemSolved(boolean problemSolved) {
        this.problemSolved = problemSolved;
    }

    @Override
    public String toString() {
        return "WorkOrder{" +
                "id=" + id +
                ", car=" + car +
                ", client=" + client +
                ", dateOfReceipt=" + dateOfReceipt +
                ", releaseDate=" + releaseDate +
                ", problemDescription='" + problemDescription + '\'' +
                ", serviceDescription='" + serviceDescription + '\'' +
                ", usedMaterijal='" + usedMaterijal + '\'' +
                ", status='" + status + '\'' +
                ", note='" + note + '\'' +
                ", totalPrice=" + totalPrice +
                ", materialPrice=" + materialPrice +
                ", materialPurchasePrice=" + materialPurchasePrice +
                ", problemSolved=" + problemSolved +
                '}';
    }
}
