package com.example.androidserviceapp.Viewer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.androidserviceapp.Model.Car;
import com.example.androidserviceapp.Model.CarManufacturer;
import com.example.androidserviceapp.Model.CarModel;
import com.example.androidserviceapp.Model.Client;
import com.example.androidserviceapp.Model.Service;
import com.example.androidserviceapp.Model.WorkOrder;
import com.example.androidserviceapp.R;
import com.example.androidserviceapp.Service.AnotherAsyncResponseInterface;
import com.example.androidserviceapp.Service.AnotherMyAsyncTask;
import com.example.androidserviceapp.Service.ApiService;
import com.example.androidserviceapp.Service.AsyncResponseInterface;
import com.example.androidserviceapp.Service.MyAsyncTask;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AddWOActivity extends AppCompatActivity implements AsyncResponseInterface, AnotherAsyncResponseInterface {

    private MyAsyncTask mt = new MyAsyncTask();
    private AnotherMyAsyncTask anohermt = new AnotherMyAsyncTask();

    private static ArrayList<Car> cars;
    private static ArrayList<Client> clients;

    private Button backBtn, addBtn;

    private Spinner carSpinner, clientSpinner;
    private EditText problemDescription, note, searchVehicle, searchClient;

    private Intent intent;

    private Car car;
    private Client client;

    private SharedPreferences sharedPreferences;
    private String address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_wo);

        mt.delegate = this;
        anohermt.delegate = this;

        // ip address
        getAddress();

        // getting cars and clients
        cars = new ArrayList<>();
        clients = new ArrayList<>();
        initCars();
        initClients();

        // search car
        searchVehicle = findViewById(R.id.searchVehicle);
        searchVehicle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(searchVehicle.getText().toString().equals("")){
                    initCars();
                }
                else{
                    ArrayList<Car> matchCars = new ArrayList<>();
                    for(Car car : cars){
                        // checking if search text matches car
                        if(car.getCarModel().getCarManufacturer().getName().toLowerCase().contains(searchVehicle.getText().toString().toLowerCase()) ||
                                car.getCarModel().getName().toLowerCase().contains(searchVehicle.getText().toString().toLowerCase()) ||
                                car.getChassisNumber().toLowerCase().contains(searchVehicle.getText().toString().toLowerCase())){
                            matchCars.add(car);
                        }
                    }
                    cars = matchCars;
                    fillCarSpinner();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        // search client
        searchClient = findViewById(R.id.searchClient);
        searchClient.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(searchClient.getText().toString().equals("")){
                    initClients();
                }
                else{
                    ArrayList<Client> matchClients = new ArrayList<>();
                    for(Client cl : clients){
                        // checking if search text matches car
                        if(cl.getName().toLowerCase().contains(searchClient.getText().toString().toLowerCase()) ||
                                cl.getLastName().toLowerCase().contains(searchClient.getText().toString().toLowerCase()) ||
                                cl.getPhone().contains(searchClient.getText().toString())){
                            matchClients.add(cl);
                        }
                    }
                    clients = matchClients;
                    fillClientSpinner();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        // listener on car and client spinner
        carSpinner = findViewById(R.id.carSpinner);
        carSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // getting a car object at selected position
                car = cars.get(pos);
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        clientSpinner = findViewById(R.id.clientSpinner);
        clientSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // getting a client object at selected position
                client = clients.get(pos);
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        // back btn
        backBtn = findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // add btn
        addBtn = findViewById(R.id.addBtn);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                problemDescription = findViewById(R.id.problemDescription);
                note = findViewById(R.id.note);

                if(problemDescription.getText().toString().equals("")){
                    Toast.makeText(AddWOActivity.this, "Opis problema ne sme biti prazan!", Toast.LENGTH_LONG).show();
                }
                else {
                    WorkOrder newWorkOrder = new WorkOrder();
                    newWorkOrder.setCar(car);
                    newWorkOrder.setClient(client);
                    newWorkOrder.setProblemDescription(problemDescription.getText().toString());
                    newWorkOrder.setNote(note.getText().toString());
                    ApiService.addWO(address + "/radniNalozi", newWorkOrder);
                    Toast.makeText(AddWOActivity.this, "Radni nalog uspesno dodat", Toast.LENGTH_LONG).show();
                    intent = new Intent(AddWOActivity.this, MainActivity.class);
                    startActivityForResult(intent, 2);
                }
            }
        });
    }

    public void getAddress(){
        sharedPreferences = getSharedPreferences("MySharedPreferences", 0);
        String address = sharedPreferences.getString("address", "");
        this.address = address;
    }

    private void initCars() {
        cars = new ArrayList<>();
        mt = new MyAsyncTask();
        mt.delegate = this;
        mt.execute(address + "/automobili_radninalozi");
    }
    private void initClients() {
        clients = new ArrayList<>();
        anohermt = new AnotherMyAsyncTask();
        anohermt.delegate = this;
        anohermt.execute(address + "/klijenti"); }

    public void fillCarSpinner(){
        carSpinner = findViewById(R.id.carSpinner);
        ArrayAdapter<Car> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, cars);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        carSpinner.setAdapter(adapter);
    }
    public void fillClientSpinner(){
        clientSpinner = findViewById(R.id.clientSpinner);
        ArrayAdapter<Client> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, clients);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        clientSpinner.setAdapter(adapter);
    }

    @Override
    public void onTaskCompleted(String output) {
        try {
            JSONArray array = new JSONArray(output);
            Gson gson = new Gson();
            if (array.length() > 0) {
                for(int i=0; i<array.length(); i++){
                    JSONObject object = array.getJSONObject(i);

                    // getting car manufacturer
                    CarManufacturer carManufacturer = gson.fromJson(object.toString(), CarManufacturer.class);
                    // getting car model
                    CarModel carModel = gson.fromJson(object.toString(), CarModel.class);
                    carModel.setCarManufacturer(carManufacturer);
                    // making car object with gson
                    Car car = gson.fromJson(object.toString(), Car.class);
                    car.setCarModel(carModel);
                    cars.add(car);
                }

            }
            fillCarSpinner();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void anotherOnTaskCompleted(String output) {
        try {
            JSONArray array = new JSONArray(output);
            Gson gson = new Gson();
            if (array.length() > 0) {
                for(int i=0; i<array.length(); i++){
                    JSONObject object = array.getJSONObject(i);

                    // getting clients
                    Client client = gson.fromJson(object.toString(), Client.class);
                    clients.add(client);
                }

            }
            fillClientSpinner();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
