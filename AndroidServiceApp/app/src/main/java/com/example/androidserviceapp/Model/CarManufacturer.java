package com.example.androidserviceapp.Model;

import com.google.gson.annotations.SerializedName;

public class CarManufacturer {

    private int id;
    @SerializedName("naziv_proizvodjaca")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }




    public CarManufacturer(){

    }
    public CarManufacturer(int id, String name){
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
