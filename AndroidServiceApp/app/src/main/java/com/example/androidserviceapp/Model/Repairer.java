package com.example.androidserviceapp.Model;

import com.google.gson.annotations.SerializedName;

public class Repairer {

    private int id;
    @SerializedName("korisnicko_ime")
    private String username;
    @SerializedName("lozinka")
    private String password;
    @SerializedName("ime_servisera")
    private String name;
    @SerializedName("prezime")
    private String lastName;
    @SerializedName("obrisan")
    private int deleted;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }


    public Repairer(String username, String password, String name, String lastName, int deleted) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.lastName = lastName;
        this.deleted = deleted;
    }
    public Repairer(){

    }

    @Override
    public String toString() {
        return "Repairer{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", deleted=" + deleted +
                '}';
    }

}
