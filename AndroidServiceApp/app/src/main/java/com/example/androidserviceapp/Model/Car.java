package com.example.androidserviceapp.Model;

import com.google.gson.annotations.SerializedName;

public class Car {

    private int id;
    CarModel carModel;
    @SerializedName("broj_sasije")
    private String chassisNumber;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CarModel getCarModel() {
        return carModel;
    }

    public void setCarModel(CarModel carModel) {
        this.carModel = carModel;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }


    public Car(){

    }

    public Car(int id, CarModel carModel, String chassisNumber){
        this.id = id;
        this.carModel = carModel;
        this.chassisNumber = chassisNumber;
    }

    @Override
    public String toString() {
        return carModel.getCarManufacturer().getName() + " " + carModel.getName() + " " + chassisNumber;
    }
}
