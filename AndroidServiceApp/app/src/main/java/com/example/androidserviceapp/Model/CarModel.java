package com.example.androidserviceapp.Model;

import com.google.gson.annotations.SerializedName;

public class CarModel {

    private int id;
    private CarManufacturer carManufacturer;
    @SerializedName("naziv_modela")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CarManufacturer getCarManufacturer() {
        return carManufacturer;
    }

    public void setCarManufacturer(CarManufacturer carManufacturer) {
        this.carManufacturer = carManufacturer;
    }

    public CarModel(){

    }
    public CarModel(int id, String name, CarManufacturer carManufacturer){
        this.id = id;
        this.name = name;
        this.carManufacturer = carManufacturer;
    }

    @Override
    public String toString() {
        return name;
    }
}
