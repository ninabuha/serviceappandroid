package com.example.androidserviceapp.Viewer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.androidserviceapp.Model.Car;
import com.example.androidserviceapp.Model.Client;
import com.example.androidserviceapp.Model.WorkOrder;
import com.example.androidserviceapp.R;
import com.example.androidserviceapp.Service.ApiService;

public class AddClientActivity extends AppCompatActivity {

    private Button backBtn, addBtn;

    private EditText name, lastName, phone;

    private Intent intent;

    private SharedPreferences sharedPreferences;
    private String address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_client);


        // ip address
        getAddress();

        // back btn
        backBtn = findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // add btn
        addBtn = findViewById(R.id.addBtn);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = findViewById(R.id.clientName);
                lastName = findViewById(R.id.clientLastName);
                phone = findViewById(R.id.clientPhone);

                Client newClient = new Client();
                newClient.setName(name.getText().toString());
                newClient.setLastName(lastName.getText().toString());
                newClient.setPhone(phone.getText().toString());
                ApiService.addClient(address + "/klijenti", newClient);
                Toast.makeText(AddClientActivity.this, "Client added successfully", Toast.LENGTH_LONG).show();
                intent = new Intent(AddClientActivity.this, ClientsActivity.class);
                startActivityForResult(intent, 2);

            }
        });
    }

    public void getAddress(){
        sharedPreferences = getSharedPreferences("MySharedPreferences", 0);
        String address = sharedPreferences.getString("address", "");
        this.address = address;
    }
}
