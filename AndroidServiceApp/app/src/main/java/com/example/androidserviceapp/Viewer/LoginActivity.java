package com.example.androidserviceapp.Viewer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.androidserviceapp.R;
import com.example.androidserviceapp.Service.AsyncResponseInterface;
import com.example.androidserviceapp.Service.LoginAsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity implements AsyncResponseInterface {

    private LoginAsyncTask lt = new LoginAsyncTask();

    private ImageView loginImg;
    private EditText usernameInput;
    private EditText passwordInput;
    private Button logInBtn;
    private ImageButton settingsIconButton;

    private Intent intent;

    private String token, role, address;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        lt.delegate = this;


        // ip address
        initAddress("http://192.168.0.25:5000");
        getAddress();

        // setting login image
        loginImg = findViewById(R.id.loginImg);
        loginImg.setImageResource(R.drawable.bmw);

        settingsIconButton = findViewById(R.id.settingsIcon);
        settingsIconButton.setImageResource(R.drawable.settings_icon);

        settingsIconButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(LoginActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        });

        // verifying user
        logInBtn = findViewById(R.id.logInBtn);
        logInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyUser();
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("RESUME");
        getAddress();
    }

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("START");
        getAddress();
    }

    public void initAddress(String address) {
        sharedPreferences = getSharedPreferences("MySharedPreferences", 0);
        editor = sharedPreferences.edit();
        editor.putString("address", address);
        editor.commit();
    }
    public void getAddress(){
        sharedPreferences = getSharedPreferences("MySharedPreferences", 0);
        String address = sharedPreferences.getString("address", "");
        this.address = address;
    }

    public void verifyUser(){
        lt = new LoginAsyncTask();
        lt.delegate = this;

        usernameInput = findViewById(R.id.usernameInput);
        passwordInput = findViewById(R.id.passwordInput);

        lt.username = usernameInput.getText().toString();
        lt.password = passwordInput.getText().toString();
        System.out.println(Base64.encodeToString(usernameInput.getText().toString().getBytes(), Base64.NO_WRAP));

        // verifying username and password
        lt.execute(address + "/login");
    }

    @Override
    public void onTaskCompleted(String output) {
        String response_status = "";
        String ok_response = "200";

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(output);
            response_status = jsonObject.get("status").toString();
            token = jsonObject.get("token").toString();
            role = jsonObject.get("role").toString();

            sharedPreferences = getSharedPreferences("MySharedPreferences", 0);
            editor = sharedPreferences.edit();
            editor.putString("token", token);
            editor.putString("role", role);
            editor.commit();

            System.out.println(response_status);
        }catch (JSONException err){
            System.out.println(err.toString());
        }


        if(ok_response.equals(response_status)){
            intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        else {
            Toast.makeText(this, "Nevalidno korisnicko ime ili lozinka!", Toast.LENGTH_LONG).show();
        }
    }
}
