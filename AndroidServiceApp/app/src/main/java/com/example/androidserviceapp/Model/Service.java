package com.example.androidserviceapp.Model;

import com.google.gson.annotations.SerializedName;

public class Service {

    private int id;
    @SerializedName("naziv_akcije")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }




    public Service(){

    }
    public Service(int id, String name){
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
