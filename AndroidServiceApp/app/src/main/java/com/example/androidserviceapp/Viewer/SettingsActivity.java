package com.example.androidserviceapp.Viewer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.androidserviceapp.R;

import java.nio.file.Files;

public class SettingsActivity extends AppCompatActivity {

    private ImageView settingsImg;
    private EditText ipAddressInput;
    private Button changeSettingsBtn, backBtn;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private String address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        getAddress();

        backBtn = findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        settingsImg = findViewById(R.id.settingsImg);
        settingsImg.setImageResource(R.drawable.settings_icon);
        ipAddressInput = findViewById(R.id.ipAddressInput);
        ipAddressInput.setText(address);

        changeSettingsBtn = findViewById(R.id.changeSettingsBtn);
        changeSettingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAddress(ipAddressInput.getText().toString());
                Toast.makeText(SettingsActivity.this, "Ip adresa je promenjena!", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getAddress(){
        sharedPreferences = getSharedPreferences("MySharedPreferences", 0);
        String address = sharedPreferences.getString("address", "");
        this.address = address;
    }

    public void setAddress(String a){
        this.address = a;
        sharedPreferences = getSharedPreferences("MySharedPreferences", 0);
        editor = sharedPreferences.edit();
        editor.putString("address", address);
        editor.commit();
    }
}
