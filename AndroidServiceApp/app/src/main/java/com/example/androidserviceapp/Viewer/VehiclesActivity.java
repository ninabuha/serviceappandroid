package com.example.androidserviceapp.Viewer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.androidserviceapp.Model.Car;
import com.example.androidserviceapp.Model.CarManufacturer;
import com.example.androidserviceapp.Model.CarModel;
import com.example.androidserviceapp.Model.Client;
import com.example.androidserviceapp.R;
import com.example.androidserviceapp.Service.ApiService;
import com.example.androidserviceapp.Service.AsyncResponseInterface;
import com.example.androidserviceapp.Service.MyAsyncTask;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VehiclesActivity extends AppCompatActivity implements AsyncResponseInterface {

    private MyAsyncTask mt = new MyAsyncTask();
    private static String token;

    private static ArrayList<Car> vehicles;
    private Button wOMenuBtn, clientsMenuBtn, vehiclesMenuBtn, addBtn;

    private LinearLayout mainLayout, vLayout;

    private ArrayList<LinearLayout> listVehicleLayout;

    private static Car temporaryVehicle;

    private Intent intent;
    private SharedPreferences sharedPreferences;
    private String address;

    private EditText searchField;
    private ImageButton searchBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicles);

        final VehiclesActivity activity = this;

        mt.delegate = this;
        getAddress();

        vehicles = new ArrayList<>();
        listVehicleLayout = new ArrayList<>();
        initVehicles();

        // changing active menu btn
        vehiclesMenuBtn = findViewById(R.id.vvehiclesBtn);
        vehiclesMenuBtn.setBackgroundColor(Color.argb(255, 255, 255, 255));

        wOMenuBtn = findViewById(R.id.vwOMenuBtn);
        wOMenuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(VehiclesActivity.this, MainActivity.class);
                startActivityForResult(intent, 2);
            }
        });

        clientsMenuBtn = findViewById(R.id.vclientsBtn);
        clientsMenuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(VehiclesActivity.this, ClientsActivity.class);
                startActivityForResult(intent, 2);
            }
        });

        searchField = findViewById(R.id.searchVField);

        // listener on search btn
        searchBtn = findViewById(R.id.searchVBtn);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(searchField.getText().toString().equals("")){
                    mt = new MyAsyncTask();
                    mt.delegate = activity;
                    mt.execute(address + "/automobili");
                }
                else{
                    ApiService.searchVehicles( address + "/automobili/pretraga/"+ searchField.getText().toString() + "/", activity);
                }
                // hiding keyboard on btn click
                try {
                    InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        });


        // listener on addBtn
        addBtn = findViewById(R.id.addVBtn);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(VehiclesActivity.this, AddVehicleActivity.class);
                startActivity(intent);
            }
        });
    }

    public void getAddress(){
        sharedPreferences = getSharedPreferences("MySharedPreferences", 0);
        String address = sharedPreferences.getString("address", "");
        this.address = address;
    }

    private void initVehicles() {
        mt.execute(address + "/automobili_radninalozi");
    }

    public static ArrayList<Car> getVehicles(){
        return vehicles;
    }
    public static void setVehicles(ArrayList<Car> vh){
        vehicles = vh;
    }

    public void drawVehicles(){
        System.out.println("draw vehicles");

        LayoutInflater inflater = LayoutInflater.from(this);
        mainLayout = findViewById(R.id.vehiclesLayout);
        mainLayout.removeAllViews();

        ImageView vehicleIcon;
        TextView vehicleManufacturer;
        TextView vehicleModel;
        TextView vehicleChassisNumber;

        int counter = 0;

        for(final Car c: vehicles){

            // filling mainLayout with vLayouts
            vLayout = (LinearLayout) inflater.inflate(R.layout.vehicle, mainLayout, false);
            listVehicleLayout.add(vLayout);

            // setting icon
            vehicleIcon = vLayout.findViewById(R.id.vehicleIcon);
            vehicleIcon.setImageResource(R.drawable.drawable_vehicle_icon);

            //
            vehicleManufacturer = vLayout.findViewById(R.id.vehicleManufacturer);
            vehicleModel = vLayout.findViewById(R.id.vehicleModel);
            vehicleChassisNumber = vLayout.findViewById(R.id.chassisNumber);

            // drawing data to view components
            vehicleManufacturer.setText(c.getCarModel().getCarManufacturer().getName());
            vehicleModel.setText(c.getCarModel().getName());
            vehicleChassisNumber.setText(c.getChassisNumber());


            System.out.println(c);
            counter +=1;

            // changing margins for last vehicle layout because of add btn
            if(vehicles.size() == counter){
                ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) vLayout.getLayoutParams();
                p.setMargins(0, 0, 0, 150);
                vLayout.requestLayout();
            }

            mainLayout.addView(vLayout);
        }
    }

    public void setTemporaryVehicle(Car c){
        temporaryVehicle = c;
    }
    public static Car getTemporaryVehicle(){
        return temporaryVehicle;
    }

    @Override
    public void onTaskCompleted(String output) {
        vehicles = new ArrayList<>();
        try {
            JSONArray array = new JSONArray(output);
            System.out.println(array);
            Gson gson = new Gson();
            if (array.length() > 0) {
                for(int i=0; i<array.length(); i++){
                    JSONObject object = array.getJSONObject(i);

                    // getting vehicles
                    // getting car manufacturer
                    CarManufacturer carManufacturer = gson.fromJson(object.toString(), CarManufacturer.class);
                    // getting car model
                    CarModel carModel = gson.fromJson(object.toString(), CarModel.class);
                    carModel.setCarManufacturer(carManufacturer);
                    // making car object with gson
                    Car car = gson.fromJson(object.toString(), Car.class);
                    car.setCarModel(carModel);
                    vehicles.add(car);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        drawVehicles();
    }
}
