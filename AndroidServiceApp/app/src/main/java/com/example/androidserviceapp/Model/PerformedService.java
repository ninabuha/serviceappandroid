package com.example.androidserviceapp.Model;

import com.example.androidserviceapp.Model.Repairer;
import com.example.androidserviceapp.Model.Service;
import com.example.androidserviceapp.Model.WorkOrder;
import com.google.gson.annotations.SerializedName;

import java.time.LocalDateTime;

public class PerformedService {

    @SerializedName("cena_izvrsene_akcije")
    private float price;
    private Repairer repairer;
    private Service service;
    private WorkOrder workOrder;
    @SerializedName("uspesna")
    private int successful;
    private LocalDateTime date;
    @SerializedName("opis_akcije")
    private String description;


    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Repairer getRepairer() {
        return repairer;
    }

    public void setRepairer(Repairer repairer) {
        this.repairer = repairer;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public WorkOrder getWorkOrder() {
        return workOrder;
    }

    public void setWorkOrder(WorkOrder workOrder) {
        this.workOrder = workOrder;
    }

    public int getSuccessful() {
        return successful;
    }

    public void setSuccessful(int successful) {
        this.successful = successful;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public PerformedService(float price, Repairer repairer, Service service, WorkOrder workOrder, int successful, LocalDateTime date, String description) {
        this.price = price;
        this.repairer = repairer;
        this.service = service;
        this.workOrder = workOrder;
        this.successful = successful;
        this.date = date;
        this.description = description;
    }

    public PerformedService(){

    }

    @Override
    public String toString() {
        return "PerformedService{" +
                "price=" + price +
                ", repairer=" + repairer +
                ", service=" + service +
                ", workOrder=" + workOrder +
                ", successful=" + successful +
                ", date=" + date +
                ", description='" + description + '\'' +
                '}';
    }
}
