import flask
from flask import request, jsonify
import jwt
from flask import current_app
from flaskext.mysql import MySQL
from pymysql.cursors import DictCursor

from functools import wraps

mysql_db = MySQL(cursorclass=DictCursor)


def secured(roles=[]):
    def secured_with_roles(f):
        @wraps(f)
        def login_check(*args, **kwargs):
            if flask.session.get("korisnik") is not None and flask.session.get("korisnik")["pravo"] in roles:
                return f(*args, **kwargs)
            return "", 401
        return login_check
    return secured_with_roles


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        print(request.headers)
        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']

        if not token:
            print("miss token")
            return jsonify({"message" : "Token is missing"}), 401


        try:
            data = jwt.decode(token, current_app.config['SECRET_KEY'])
            print(data)

        except:
            print("inv token")
            return jsonify({'message' : 'Token is invalid'}), 401

        return f(*args, **kwargs)

    return decorated
